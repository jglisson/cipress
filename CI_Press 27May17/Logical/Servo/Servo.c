/********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Program: Servo
 * File: Servo.c
 * Author: KK
 * Created: August 30, 2011
 ********************************************************************
 * Implementation of program Servo
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif

//#include <globdef.h>
#include <math.h>
#include <..\Glob\globaldef.h>
#include <..\Glob\globaldecl.h>


#define W_NET_INIT		   0            /* Defines for the step sequencer */

#define AUT_VAX_STOP        1
#define AUT_STOP           	  2

#define SW_CTRL_OFF        5
#define W_CTRL_OFF        10
#define GLOBAL_INIT       20
#define W_GLOBAL_INIT     30
#define SEND_PARAM_TABLE  40
#define PLKU_ENABLE       41
#define SEND_CAM		  50
#define SW_CTRL_ON        60
#define W_CTRL_ON         70
#define HOME_START_0      80
#define HOME_START        81
#define W_HOME_OK         90
#define COMMAND          100
#define POS_MOVEMENT     110
#define NEG_MOVEMENT     120
#define ABS_MOVEMENT     130
#define REL_MOVEMENT     140
#define W_IN_POS         150
#define STOP_MOVEMENT    160
#define LIMIT_INIT		 165
#define W_STOP_MOVEMENT  170
#define W_STOP			 171
#define BASIS_INIT		 180	
#define W_BASIS_INIT	 181
#define W_MOVE_DONE		 200
#define W_JOG_DONE		 210
#define W_LIMIT_INIT	 212


#define CHCK_COM_OFFSET0 220
#define CHCK_COM_OFFSET1 221
#define CHCK_COM_OFFSET2 222
#define CHCK_COM_OFFSET3 223
#define CHCK_COM_OFFSET4 224
#define CHCK_COM_OFFSET5 225

#define W_VAX_HOME		 230

#define ERROR_STEP       250
#define W_STOP_MOVEMENT2 251
#define W_ERROR_CLR      252
	


void AssignJogParam(ACP_typ* gAxis);
void GetParam(ACP_typ* gAxis);
void GetServoCfgParam(int i);
DINT TGo( TON_typ *timer, REAL timeoutTime ) ;
BOOL TDone( TON_typ *timer );

 



 /* GLOBAL VARIABLES: */

/* LOCAL VARIABLES */
_LOCAL BOOL ParmTableDone[SERVO_NUM];



void _INIT ServoINIT( void )
{
UINT i;
	

	
	for (i = 0 ; i < SERVO_NUM ; i++){	
		
		gAxis[i].msc.step = 0;
		gAxis[i].msc.errorStep = 0;
		gAxis[i].msc.plnkStep = 0;
		gAxis[i].msc._cycusdatfr = 0;
		gAxis[i].msc._cycusdatto = 0;

		ServoCurCfg[i].homePos = 0;
		
		
	}	

	
	
}

void _CYCLIC ServoCYCLIC( void )
{	
int i;
ACP10AXIS_typ *p_ax_dat;
ACP_Cfg_typ* cfg;
ACP_CurCfg_typ* cur;
DINT _dint;




	/** COMMON STEPS **/

	for (i = 0 ; i < SERVO_NUM ; i++){

		
		if(CfgReady && init_Status[i]== 1){
			
			p_ax_dat = (ACP10AXIS_typ*) gAxis[i].msc.pAxis;	
			
			cfg = gAxis[i].msc.pCfg;
			cur = gAxis[i].msc.pCurCfg;

			if(gAxis[i].Param.Torq == 0)
				gAxis[i].Param.Torq = 100;


			
			if (gAxis[i].Status.Error && gAxis[i].msc.step < ERROR_STEP)
				gAxis[i].msc.step = ERROR_STEP;
			
			else if((gAxis[i].Command.Stop || gAxis[i].Command.Halt) && gAxis[i].Status.Homed && (gAxis[i].msc.step != W_STOP_MOVEMENT)){	

				if(	gAxis[i].Command.Halt)			
					gAxis[i].xCMD = acpMOVE_HALT;
				else
					gAxis[i].xCMD = acpMOVE_STOP;
				
				gAxis[i].msc.step = W_STOP_MOVEMENT;
				
			}
			
			else if(gAxis[i].Command.VaxStop){
			
				gAxis[i].Command.VaxStop = 0;
				gAxis[i].xCMD = acpVAX_STOP;
				
			}
			else if(gAxis[i].Command.VaxHome){
				
				gAxis[i].Command.VaxHome = 0;
				gAxis[i].xCMD = acpVAX_HOME;
				
			}
			else if(gAxis[i].Command.VaxMoveVelocity){
				
				gAxis[i].Command.VaxMoveVelocity = 0;				
				gAxis[i].xCMD = acpVAX_JOG_POS;
			}
			else if(gAxis[i].Command.VaxMoveAdditive){

				
				gAxis[i].Command.VaxMoveAdditive = 0;				
				gAxis[i].xCMD = acpVAX_REL_MOVE;
			}
			

			switch(gAxis[i].msc.step){

				/**/
				case W_NET_INIT:

					strcpy(gAxis[i].Status.ErrorText.line1 , "Network Not Ready ");

					if ( gAxis[i].Status.Network_Init && (gAxis[i].xCMD == 0))						
					{					
						gAxis[i].Command.ErrorAckAll = 1;

						strcpy(gAxis[i].Status.ErrorText.line1 , "");
						TGo(&gAxis[i].msc.tmr1, 0.5);

						
						gAxis[i].xCMD = acpVAX_STOP;
						gAxis[i].msc.step = AUT_VAX_STOP;
					}
				break;


				case AUT_VAX_STOP:

					if(!gAxis[i].xCMD){

						gAxis[i].xCMD = acpMOVE_HALT;
						gAxis[i].msc.step = AUT_STOP;
					}	
						

				break;	

				case AUT_STOP:

					if(gAxis[i].Status.MoveMode == ncOFF && TDone(&gAxis[i].msc.tmr1)){
						gAxis[i].xCMD = acpAUT_STOP;
						gAxis[i].msc.step = SW_CTRL_OFF;
					}						

				break;	


				/**/
				case SW_CTRL_OFF:

					if(gAxis[i].Status.MoveMode == ncOFF){
						
						gAxis[i].msc.step = W_CTRL_OFF;	
					}
					
				break;

				case W_CTRL_OFF:

					if(!gAxis[i].Status.ServoON)
						gAxis[i].msc.step = GLOBAL_INIT;

				break;	

				/**/
				case GLOBAL_INIT:

					GetServoCfgParam(i);
					gAxis[i].xCMD = acpGLOBAL_INIT;
					gAxis[i].msc.step = W_GLOBAL_INIT;
					
				break;

				case W_GLOBAL_INIT:

					if(gAxis[i].Status.Global_Init){
						
						gAxis[i].msc.i = 0;												
						gAxis[i].msc.step = SEND_CAM;

						TGo(&gAxis[i].msc.tmr1, 1.0);

					}
					
				break;			

				/**/
				case SEND_CAM:

					if(gAxis[i].xCMD == 0 && TDone(&gAxis[i].msc.tmr1)){

						if(cfg->CamName[gAxis[i].msc.i][0]!=0){
						
							gAxis[i].Param.pParTableName = cfg->CamName[gAxis[i].msc.i];
							gAxis[i].msc.i++;
							gAxis[i].Param.CamID = gAxis[i].msc.i;
							gAxis[i].xCMD = acpCAM_DOWNLOAD;

							TGo(&gAxis[i].msc.tmr1, 1.0);
						}
						else{
							gAxis[i].msc.i = 0;
							gAxis[i].msc.step = SEND_PARAM_TABLE;
							TGo(&gAxis[i].msc.tmr1, 0.5);
						}
					}
				
				break;	

				/**/
				case SEND_PARAM_TABLE:

					if(gAxis[i].xCMD == 0 && TDone(&gAxis[i].msc.tmr1)){
						
						if(cfg->ParTableName[gAxis[i].msc.i][0]!=0){
							
							gAxis[i].Param.pParTableName = cfg->ParTableName[gAxis[i].msc.i];
							gAxis[i].msc.i++;
							gAxis[i].xCMD = acpPAR_DOWNLOAD;
							
							TGo(&gAxis[i].msc.tmr1, 2.0);
						}
						
						else
							gAxis[i].msc.step = PLKU_ENABLE;
					}

				break;

				case PLKU_ENABLE:

					if(TDone(&gAxis[i].msc.tmr1)){
						if(gAxis[i].xCMD == 0){					
							gAxis[i].xCMD = acpPLKU_ENABLE;
							gAxis[i].msc.step = SW_CTRL_ON;
												
							ParmTableDone[i] = 1;
						}
					}
						

				break;
				
				
				/**/
				case SW_CTRL_ON:

					if(gAxis[i].xCMD == 0){	

						if(gAxis[i].Command.Power){
							memset(&gAxis[i].Command, 0 , sizeof(gAxis[i].Command));
							gAxis[i].Command.Power = 1;
							gAxis[i].xCMD = acpCONTROLLER_ON;
							gAxis[i].msc.step = W_CTRL_ON;
						}
						else if(!gAxis[i].Command.Power && gAxis[i].Status.ServoON){

							if(!gAxis[i].xCMD)
								gAxis[i].xCMD = acpCONTROLLER_OFF;	

						}	
					}
					
				break;

				case W_CTRL_ON:		

					if(gAxis[i].xCMD == 0){	
						if(gAxis[i].Status.ServoON){
							/**
							if(gAxis[i].Status.Homed)
								gAxis[i].msc.step = COMMAND;
							else
								gAxis[i].msc.step = HOME_START;
							**/
							gAxis[i].msc.step = HOME_START;
							
						}

					}
					
				break;			


				/**/
				case HOME_START:

					if(gAxis[i].xCMD == 0){	

						p_ax_dat->move.homing.parameter.mode = cfg->home_mode;						
						
						gAxis[i].Param.HomePosition = gAxis[i].msc.pCurCfg->homePos;						
						
						gAxis[i].xCMD = acpHOME;
						
						gAxis[i].msc.step = W_HOME_OK;
					}
					

				break;	

				case W_HOME_OK:

					if(gAxis[i].xCMD == 0){	

						if(gAxis[i].Status.Homed){

							TGo(&gAxis[i].msc.tmr1, 0.5);
							gAxis[i].msc.step = COMMAND;
						}
					}
				break;


				case W_LIMIT_INIT:
						if(TDone(&gAxis[i].msc.tmr1) &&
							p_ax_dat->limit.init ){
							gAxis[i].Command.Home = 0;
							gAxis[i].msc.step = COMMAND;
						}
				break;		
				

				/// COMMANDS 
				case COMMAND:					

					if(gAxis[i].xCMD == 0){

						
						if(!gAxis[i].Command.Power){
							memset(&gAxis[i].Command, 0 , sizeof(gAxis[i].Command));
							if(gAxis[i].Status.MoveMode!=ncOFF){
								gAxis[i].xCMD = acpMOVE_HALT;
								gAxis[i].msc.step = W_STOP_MOVEMENT;
							}	
							else{
								gAxis[i].xCMD = acpCONTROLLER_OFF;
								gAxis[i].msc.step = SW_CTRL_ON;
							}
						}
							
						else {							

							if(gAxis[i].Command.MoveAbsolute){								
								
								p_ax_dat->move.basis.parameter.s     = gAxis[i].Param.Position;
								GetParam(&gAxis[i]);
								
								gAxis[i].xCMD = acpABS_MOVE;

								gAxis[i].msc.step = W_MOVE_DONE;

								
							}
							else if(gAxis[i].Command.MoveAdditive){

								p_ax_dat->move.basis.parameter.s     = gAxis[i].Param.Distance;
								GetParam(&gAxis[i]);
						
								gAxis[i].xCMD = acpREL_MOVE;

								gAxis[i].msc.step = W_MOVE_DONE;
								
							}
							else if(gAxis[i].Command.MoveVelocity){								
								
								p_ax_dat->move.basis.parameter.v_pos = fabsf(gAxis[i].Param.Velocity);
								//p_ax_dat->move.basis.parameter.v_pos = gAxis[i].Param.Velocity;
								p_ax_dat->move.basis.parameter.v_neg  = p_ax_dat->move.basis.parameter.v_pos;
								
								p_ax_dat->move.basis.parameter.a1_neg = gAxis->Param.Acceleration;
								p_ax_dat->move.basis.parameter.a1_pos = p_ax_dat->move.basis.parameter.a1_neg;

								p_ax_dat->move.basis.parameter.a2_neg = gAxis->Param.Deceleration;
								p_ax_dat->move.basis.parameter.a2_pos = p_ax_dat->move.basis.parameter.a2_neg;

								gAxis[i].msc.step = W_MOVE_DONE;

								

								if(gAxis[i].Param.Velocity > 0)
									gAxis[i].xCMD = acpJOG_POS;
								else if(gAxis[i].Param.Velocity < 0)
									gAxis[i].xCMD = acpJOG_NEG;	
								else {
									gAxis[i].Command.MoveVelocity = 0;
									gAxis[i].msc.step = COMMAND;									
								}
								
								
							}
							
							else if(gAxis[i].Command.MoveJogPos){							
								
								AssignJogParam(&gAxis[i]);
								gAxis[i].xCMD = acpJOG_POS;

								gAxis[i].msc.step = W_JOG_DONE;
								
							}
							else if(gAxis[i].Command.MoveJogNeg){								
								
								AssignJogParam(&gAxis[i]);
								gAxis[i].xCMD = acpJOG_NEG;

								gAxis[i].msc.step = W_JOG_DONE;

								
							}	

							else if(gAxis[i].Command.AutomatStart){	

								gAxis[i].Command.AutomatStart = 0;

								if(gAxis[i].Status.MoveMode != ncACTIVE)
									gAxis[i].xCMD = acpAUT_START;
								
							}	

							else if(gAxis[i].Command.AutomatStop){	

								gAxis[i].Command.AutomatStop = 0;

								if(gAxis[i].Status.AutState != 255)
									gAxis[i].xCMD = acpAUT_STOP;
								
							}	

							else if(gAxis[i].Command.Home){	

								gAxis[i].Param.HomePosition = 0;
								
								gAxis[i].msc.pCurCfg->homePos -= p_ax_dat->monitor.s;									
								gAxis[i].xCMD = acpHOME_DIRECT;
								
								gAxis[i].msc.step = W_HOME_OK;

							}	

							else if(gAxis[i].Command.Cyc_Abs_Start){

								gAxis[i].xCMD = acpMOVE_HALT;
								gAxis[i].msc.step = W_STOP_MOVEMENT	;								

							}	

							else if(gAxis[i].Command.Cyc_Vel_Start){

								gAxis[i].xCMD = acpMOVE_HALT;
								gAxis[i].msc.step = W_STOP_MOVEMENT;						

							}	
						}	
					}
						
					

				break;


				case W_STOP_MOVEMENT:

					if((gAxis[i].Status.MoveMode == ncOFF || gAxis[i].Status.MoveMode == ncSTOP 
						//|| gAxis[i].Status.MoveMode == ncMOVE_CMD) && (gAxis[i].xCMD == 0) ){
						) && (gAxis[i].xCMD == 0) ){
						gAxis[i].Command.MoveAbsolute = 0;
						gAxis[i].Command.MoveAdditive = 0;
						gAxis[i].Command.MoveJogPos = 0;
						gAxis[i].Command.MoveJogNeg = 0;
						gAxis[i].Command.MoveVelocity = 0;
						gAxis[i].Command.Stop = 0;
						gAxis[i].Command.Halt = 0;

						if(gAxis[i].Command.Cyc_Abs_Start || gAxis[i].Command.Cyc_Vel_Start){	

							p_ax_dat->move.basis.parameter.a1_neg = p_ax_dat->limit.parameter.a1_neg;
							p_ax_dat->move.basis.parameter.a2_neg = p_ax_dat->limit.parameter.a2_neg;
							p_ax_dat->move.basis.parameter.a1_pos = p_ax_dat->limit.parameter.a1_pos;
							p_ax_dat->move.basis.parameter.a2_pos = p_ax_dat->limit.parameter.a2_pos;
							p_ax_dat->move.basis.parameter.v_pos  = p_ax_dat->limit.parameter.v_pos;
							p_ax_dat->move.basis.parameter.v_neg  = p_ax_dat->limit.parameter.v_neg;
													
							gAxis[i].xCMD = acpBMI2;

							
							TGo(&gAxis[i].msc.tmr1, 1.0);

							gAxis[i].msc.step = W_BASIS_INIT;

						}
						else
							gAxis[i].msc.step = COMMAND;
							
					}
					
				break;

				case W_MOVE_DONE:

					if(gAxis[i].Status.MoveMode == ncOFF){
						gAxis[i].Command.MoveAbsolute = 0;
						gAxis[i].Command.MoveAdditive = 0;
						gAxis[i].Command.MoveVelocity = 0;
						gAxis[i].msc.step = COMMAND;
					}
					
				break;

				case W_JOG_DONE:
					
					if(!(gAxis[i].Command.MoveJogPos || gAxis[i].Command.MoveJogNeg)){
						gAxis[i].xCMD = acpJOG_STOP;
						gAxis[i].msc.step = W_STOP_MOVEMENT;
					}
					
				break;					

				case W_BASIS_INIT:

					gAxis[i].Param.Cyc_Pos  = gAxis[i].Status.ActPosition;
					gAxis[i].Param.Cyc_Vel  = gAxis[i].Status.ActVelocity;	
				

					if(TDone(&gAxis[i].msc.tmr1)&& p_ax_dat->move.basis.init ){

						if(gAxis[i].Command.Cyc_Abs_Start){

							gAxis[i].Command.Cyc_Abs_Start = 0;							
							gAxis[i].Param.ParID = ACP10PAR_CMD_CYC_ABS_MOVE;
							gAxis[i].Param.ParID_Data = ncSTART;							
							gAxis[i].xCMD = acpSET_PARID;						

						}	

						else if(gAxis[i].Command.Cyc_Vel_Start){

							gAxis[i].Command.Cyc_Vel_Start = 0;													
							gAxis[i].Param.ParID = ACP10PAR_CMD_CYC_V_MOVE;
							gAxis[i].Param.ParID_Data = ncSTART;							
							gAxis[i].xCMD = acpSET_PARID;						

						}	
						gAxis[i].msc.step = COMMAND;
					}	

				break;	
				
				
				
				/**/


				/// ERROR 
				case ERROR_STEP:

					memset(&gAxis[i].Command, 0 , sizeof(gAxis[i].Command));

					TGo(&gAxis[i].msc.tmr1, 1.0);
					//Alarms[i] = 1;

					if(gAxis[i].xCMD == 0){


						/*
						if(gAxis[i].Status.MoveMode!= ncOFF || (gAxis[i].Status.MoveMode != ncSTOP) ){ 
							
							gAxis[i].xCMD = acpMOVE_STOP;
							
						}
						else if(gAxis[i].Status.ServoON && gAxis[i].Status.MoveMode == ncOFF){

							gAxis[i].xCMD = acpCONTROLLER_OFF;							
						}						
						else if(!gAxis[i].Status.ServoON && gAxis[i].Status.MoveMode == ncOFF)
							gAxis[i].msc.step = W_STOP_MOVEMENT2;
						*/

						//gAxis[i].xCMD = acpMOVE_STOP;
						gAxis[i].xCMD = acpMOVE_HALT;						
						gAxis[i].msc.step = W_STOP_MOVEMENT2;
						
					}

					

				break;

				case W_STOP_MOVEMENT2:

					if(gAxis[i].xCMD == 0){

						if(gAxis[i].Status.MoveMode == ncOFF){	

							gAxis[i].xCMD = acpCONTROLLER_OFF;	
							gAxis[i].msc.step = W_ERROR_CLR;
						}

					}
					
				break;

				case W_ERROR_CLR:					

					if(TDone(&gAxis[i].msc.tmr1)){

						if(!gAxis[i].Status.Error){
							
							if(gAxis[i].Status.Homed){
										
								gAxis[i].msc.step = SW_CTRL_ON;
							}
							else
								gAxis[i].msc.step = W_NET_INIT;

								
						}				
				
					}
					
				break;	
				

				/************************************************************/
				/************************************************************/
				default:
					
					gAxis[i].msc.step = W_ERROR_CLR;
					
				break;	
				
		
				
				
			}

			
			
		
			
		}
		else if(gAxis[i].Status.Network_Init && gAxis[i].Status.MoveMode != ncOFF){
			gAxis[i].xCMD = acpMOVE_STOP;
			}
		else if(gAxis[i].Status.Network_Init && gAxis[i].Status.MoveMode == ncOFF && gAxis[i].Status.ServoON){
			gAxis[i].xCMD = acpCONTROLLER_OFF;
			}
		else{
			gAxis[i].msc.step = W_NET_INIT;

		}		
		/* call server */
		ACPx_Serv(&gAxis[i]);

		
	}

	
}  /*end _CYCLIC*/


	void AssignJogParam(ACP_typ* gAxis){
		ACP10AXIS_typ *axis;

		if(gAxis->msc.pAxis){
			axis = (ACP10AXIS_typ*) gAxis->msc.pAxis;	

			
			axis->move.basis.parameter.v_pos  = gAxis->Param.JogVelocity;
			axis->move.basis.parameter.v_neg  = axis->move.basis.parameter.v_pos;
			
			axis->move.basis.parameter.a1_neg = gAxis->Param.JogAccDec;
			axis->move.basis.parameter.a1_pos = axis->move.basis.parameter.a1_neg;

			axis->move.basis.parameter.a2_neg = axis->move.basis.parameter.a1_neg;
			axis->move.basis.parameter.a2_pos = axis->move.basis.parameter.a2_neg;
			
		}
		

		return;
	}
	void GetParam(ACP_typ* gAxis){
		ACP10AXIS_typ *axis;

		if(gAxis->msc.pAxis){
			axis = (ACP10AXIS_typ*) gAxis->msc.pAxis;	

			if(gAxis->Param.Velocity > 0){
				axis->move.basis.parameter.v_pos  = gAxis->Param.Velocity;
				axis->move.basis.parameter.v_neg  = axis->move.basis.parameter.v_pos;
			}
			else{
				axis->move.basis.parameter.v_pos  = axis->limit.parameter.v_pos;
				axis->move.basis.parameter.v_neg  = axis->limit.parameter.v_neg;
			}

			if(gAxis->Param.Acceleration > 0){
				axis->move.basis.parameter.a1_neg = gAxis->Param.Acceleration;
				axis->move.basis.parameter.a1_pos = gAxis->Param.Acceleration;
			}
			else{
				axis->move.basis.parameter.a1_neg = axis->limit.parameter.a1_neg;
				axis->move.basis.parameter.a1_pos = axis->limit.parameter.a1_pos;
			}

			if(gAxis->Param.Deceleration ){
				axis->move.basis.parameter.a2_neg = gAxis->Param.Deceleration;
				axis->move.basis.parameter.a2_pos = gAxis->Param.Deceleration;
			}
			else{
				axis->move.basis.parameter.a2_neg = axis->limit.parameter.a2_neg;
				axis->move.basis.parameter.a2_pos = axis->limit.parameter.a2_pos;
			}
			
		}
	}
	

	void GetServoCfgParam(int i){
	ACP10AXIS_typ *p_axis;

		if(!gAxis[i].msc.pAxis)
			return;
		
		p_axis = (ACP10AXIS_typ*) gAxis[i].msc.pAxis;

		p_axis->limit.parameter.a1_neg = ServoCfg[i].a_max;
		p_axis->limit.parameter.a2_neg = ServoCfg[i].a_max;
		p_axis->limit.parameter.a1_pos = ServoCfg[i].a_max;
		p_axis->limit.parameter.a2_pos = ServoCfg[i].a_max;


		p_axis->limit.parameter.v_pos = ServoCfg[i].v_max;
		p_axis->limit.parameter.v_neg = ServoCfg[i].v_max;


		p_axis->encoder_if.parameter.scaling.load.units = ServoCfg[i].units;
		p_axis->encoder_if.parameter.scaling.load.rev_motor = ServoCfg[i].rev_motor;
		p_axis->encoder_if.parameter.count_dir = ServoCfg[i].count_dir;		
		//ServoCfg[i].unit_precision;
		p_axis->limit.parameter.pos_sw_end = ServoCfg[i].pos_sw_end;
		p_axis->limit.parameter.neg_sw_end = ServoCfg[i].neg_sw_end;

		p_axis->move.homing.parameter.mode = ServoCfg[i].home_mode;
 

	return;
	}
	
	
	DINT TGo( TON_typ *timer, REAL timeoutTime ) {
		timer->IN = 0;
		TON(timer);
		timer->IN = 1;
		timer->PT = (int) ((timeoutTime * 1000.0)+0.5);
		TON(timer);
		return 1;
	}
	
	BOOL TDone( TON_typ *timer ) {
		return timer->Q;
	}

	
	
	
