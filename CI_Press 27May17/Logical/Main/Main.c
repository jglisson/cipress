/*********************************************************
* FILE	 		:	
*
* OBJECTS		:
*
* OVERVIEW		:
*
* HISTORY 		:
*
* DEPENDANCIES	:
*********************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <math.h>
#include <..\Glob\globaldef.h>
#include <..\Glob\globaldecl.h>

_LOCAL ACP10AXIS_typ *axis;
_LOCAL ACP10VAXIS_typ *vaxis;

void CFG_HardCode();
void MovePosRelative(MACH_obj *m , ACP_typ *ax , float x , float v , float a);
void MoveNegRelative(MACH_obj *m , ACP_typ *ax , float x , float v , float a);
void Ilum_Btn_srv(ILUM_BTN_obj *btn);

_LOCAL plcbit KK_Test;


void _INIT ProgramInit(void)
{
	Mach.pCurCfg = &CurCfg;

	Ilum_Buttons[0].Out_Color[0] = BTN_YELLOW;
	Ilum_Buttons[0].Out_Color[1] = BTN_GRAY;
	Ilum_Buttons[0].Out_Color[2] = BTN_GREEN;
	Ilum_Buttons[0].Out_Color[5] = BTN_RED;
	
	
	/*

	Mach.pCurCfg->Color_Active[0] = 1;
	Mach.pCurCfg->Color_Active[1] = 1;
	Mach.pCurCfg->Color_Active[2] = 1;
	Mach.pCurCfg->Color_Active[3] = 1;
	Mach.pCurCfg->Color_Active[4] = 1;

	
	// USINT PrintCylind_Mode[COLOR_NUM] ;
	// USINT AniloxRoller_Mode[COLOR_NUM];	
	// USINT DrBlade_Mode[COLOR_NUM];
	

	Mach.pCurCfg->Mode = 1; // Virtual Main Shaft
	Mach.pCurCfg->Color5Dir = 1;
	Mach.pCurCfg->GlobalAnilox_Circum = 100.0;	// mm
	// Anilox_Dim[COLOR_NUM];
	
	Mach.pCurCfg->BlankLen = 100.0; // mm
	
	Mach.pCurCfg->MainVaxSpeed = 120.0; //RPM
	
	
	Mach.pCurCfg->CoarseDist = 5.0; // mm
	Mach.pCurCfg->FineDist = 1.0; // mm
	
	Mach.pCurCfg->IncrVel = 1.0; // mm/sec
	Mach.pCurCfg->AlignVel = 5.0;	// mm/sec
	Mach.pCurCfg->AniloxVel = 60.0; // RPM

	Mach.pCurCfg->AngageDly = 1.0;
	Mach.pCurCfg->AngageVel = 20.0; //RPM

	

	Mach.pCurCfg->Mode = 0; // Virtual Main Shaft
	Mach.pCurCfg->Color5Dir = 1;

	*/

	if(Mach.pCurCfg->GlobalAnilox_Circum <= 0.0)
		Mach.pCurCfg->GlobalAnilox_Circum = 100.0;	// mm
	// Anilox_Dim[COLOR_NUM];

	if(Mach.pCurCfg->BlankLen <= 0.0)
		Mach.pCurCfg->BlankLen = 100.0; // mm

	if(Mach.pCurCfg->MainVaxSpeed < 10.0)
		Mach.pCurCfg->MainVaxSpeed = 120.0; //RPM
	
	if(Mach.pCurCfg->CoarseDist <= 0.0)
		Mach.pCurCfg->CoarseDist = 5.0; // mm

	if(Mach.pCurCfg->FineDist <= 0.0)	
		Mach.pCurCfg->FineDist = 1.0; // mm
	
	if(Mach.pCurCfg->IncrVel <= 0.0)
		Mach.pCurCfg->IncrVel = 1.0; // mm/sec

	if(Mach.pCurCfg->AlignVel <= 0.0)
		Mach.pCurCfg->AlignVel = 5.0;	// mm/sec

	if(Mach.pCurCfg->JogVel <= 0.0)
		Mach.pCurCfg->JogVel = 5.0;	// mm/sec	

	if( Mach.pCurCfg->AniloxVel <= 0.0)	
		Mach.pCurCfg->AniloxVel = 60.0; // RPM

	Mach.pCurCfg->AngageDly = 1.0;
	Mach.pCurCfg->AngageVel = 20.0; //RPM
	
	
	// Hardcoded now
	Mach.pCurCfg->MainVaxAcc = FULL_ROT * 2.0;
	Mach.pCurCfg->AniloxAcc = FULL_ROT * 10.0;
	Mach.pCurCfg->AlignAcc = FULL_ROT * 10.0;
	Mach.pCurCfg->IncrAcc = FULL_ROT * 10.0;
	Mach.pCurCfg->JogAcc = FULL_ROT * 10.0;
	
	strcpy(PLC_Version , "CI_17Mar17A");
	CFG_HardCode();

	//mAXIS[IMPR_CYLIND].Cmd.Synchro 	= 1;
	mAXIS[IMPR_CYLIND].Cmd.Active	= 1;
	//mAXIS[INSIDE_CYLIND].Cmd.Synchro = 1;
	mAXIS[INSIDE_CYLIND].Cmd.Active   = 1;
	
	
	
	KK_temp_Length = 100.0;

}

void _CYCLIC ProgramCyclic(void){
int i , k;
UINT action_status;
UINT ready , readyJog;


	Mach.pCurCfg = &CurCfg;


	////////////////////////////////////////////////
	//  Masetr Virtual Machine
	if(gAxis[M_SHAFT].msc.pVaxis){

		vaxis = (ACP10AXIS_typ*) gAxis[INSIDE_CYLIND].msc.pVaxis;

		if(Ilum_Buttons[0].Input[2] && !Ilum_Buttons[0]._Input[2])
			Mach.Cmd.MainShaft = MS_RUN;
		
		else if(Ilum_Buttons[0].Input[5] && !Ilum_Buttons[0]._Input[5])
			Mach.Cmd.MainShaft = MS_STOP;
		
		else if(Ilum_Buttons[0].Input[1] && !Ilum_Buttons[0]._Input[1])
			Mach.Cmd.MainShaft = MS_STEP;
			
			
		
		if(gAxis[INSIDE_CYLIND].Status.Global_Init){


			switch(Mach.Cmd.MainShaft){


				case DONE:


				break;

				case MS_STOP:

					gAxis[INSIDE_CYLIND].Command.VaxStop = 1;	
					Mach.Cmd.MainShaft = DONE;


				break;


				case MS_RUN:

					if(gAxis[INSIDE_CYLIND].Status.VaxMoveMode == ncOFF){


						vaxis->move.basis.parameter.v_pos =  (Mach.pCurCfg->MainVaxSpeed / 60.0) * FULL_ROT;
						vaxis->move.basis.parameter.a1_pos = Mach.pCurCfg->MainVaxAcc;
						vaxis->move.basis.parameter.a2_pos = vaxis->move.basis.parameter.a1_pos;
						
						gAxis[INSIDE_CYLIND].Command.VaxMoveVelocity = 1;
					}	
					Mach.Cmd.MainShaft = DONE;
							


				break;



				case MS_STEP:

					if(gAxis[INSIDE_CYLIND].Status.VaxMoveMode == ncOFF){

						vaxis->move.basis.parameter.s = FULL_ROT;
						vaxis->move.basis.parameter.v_pos =  (Mach.pCurCfg->MainVaxSpeed / 60.0) * FULL_ROT;
						vaxis->move.basis.parameter.a1_pos = Mach.pCurCfg->MainVaxAcc;
						vaxis->move.basis.parameter.a2_pos = vaxis->move.basis.parameter.a1_pos;
						
						gAxis[INSIDE_CYLIND].Command.VaxMoveAdditive = 1;
					}	
					Mach.Cmd.MainShaft = DONE;

				break;	



			}
		}	

		if(Mach.pCurCfg->Mode)		
			Mach.RPM = (gAxis[M_SHAFT].Status.ActVelocity * 60.0)/ FULL_ROT;
		else
			Mach.RPM = (vaxis->monitor.v * 60.0)/ FULL_ROT;
	}
	////////////////////////////////////////////////

	

	switch(Mach.shft_step){


		case 0:

			if(Mach.RPM >= Mach.pCurCfg->AngageVel){

				TimerStart(&Mach.Tmr , Mach.pCurCfg->AngageDly);
				
				Mach.shft_step++;
			}
			
		break;

		case 1:

			if(Mach.RPM < Mach.pCurCfg->AngageVel)
				Mach.shft_step = 3;
			
			else if(TimerIsTimeOut(&Mach.Tmr))
				Mach.shft_step++;
		break;	


		case 2:
			
			if(Mach.RPM < Mach.pCurCfg->AngageVel )
				Mach.shft_step++;

						
		break;

		case 3:

			TimerStart(&Mach.Tmr , Mach.pCurCfg->AniloxDly);
			Mach.shft_step++;

		break;


		case 4:

			if(TimerIsTimeOut(&Mach.Tmr))
				Mach.shft_step = 0;


		break;		

	}

	TimerExecute(&Mach.Tmr);

	Mach.Cmd.Engage = (Mach.shft_step == 2);
	Mach.Cmd.AniloxRun = (Mach.shft_step == 0);
		
	/////////////// Alarms ////////////////
	

	for ( i = ANILOX_1 ; i <= ANILOX_5; i++){

		k = i -COLOR_1;
		
		if(Mach.pCurCfg->Color_Active[k]){
			
			if(gAxis[i].Status.Error )
				Screen.Alarms[i+1];
		}	

	}

	for ( i = COLOR_1 ; i <= COLOR_5; i++){

		k = i -COLOR_1;
		
		if(Mach.pCurCfg->Color_Active[k]){
			
			if(gAxis[i].Status.Error )
				Screen.Alarms[i+1];
		}	

	}


	for ( i = IMPR_CYLIND; i <= INSIDE_CYLIND; i++){

		if(gAxis[i].Status.Error )
			Screen.Alarms[i+1];

	}

	Mach.Alarm = 0;
	for( i = 0 ; i < 16 ; i++){

		if(Screen.Alarms[i])
			Mach.Alarm = 1;

	}
	
	if(Screen.MustRestart){

		Mach.Alarm = 1;
		Screen.Alarms[0] = 1;

	}	

	//////////////////////////////////////////

	if(!Mach.ESTOP && !Mach.Alarm){

		mAXIS[IMPR_CYLIND].Cmd.Synchro 		= 1;
		mAXIS[INSIDE_CYLIND].Cmd.Synchro 	= 1;

		for(i = 0 ; i < COLOR_NUM ; i++){

			if(Mach.pCurCfg->Color_Active[i]){

				mAXIS[i+COLOR_1].Cmd.Synchro = 1;
				mAXIS[i+ANILOX_1].Cmd.Synchro = 1;

			}else{

				mAXIS[i+COLOR_1].Cmd.Synchro = 0;
				mAXIS[i+ANILOX_1].Cmd.Synchro = 0;
			}
			
		}
	}
	else{

		mAXIS[IMPR_CYLIND].Cmd.Synchro 		= 0;
		mAXIS[INSIDE_CYLIND].Cmd.Synchro 	= 0;

		for(i = 0 ; i < COLOR_NUM ; i++){	
			
			mAXIS[i+COLOR_1].Cmd.Synchro = 0;
			mAXIS[i+ANILOX_1].Cmd.Synchro = 0;
			
		}
	}
	

	///////  PRINT Cylinders ///////////////////////

	for ( i = COLOR_1; i <= COLOR_5; i++)	{

		k = i -COLOR_1;

		if(!Mach.pCurCfg->Color_Active[k])			
			break;

		
		
		vaxis = (ACP10AXIS_typ*) gAxis[i].msc.pVaxis;
		//  Relative Movement - FWD & REV	
		
		if(gAxis[i].msc.step != 0){


			//mAXIS[i].Cmd.JogFWD = 0;
			
			switch(Mach.Cmd.PrintCylind[k] ){


				case CMD_FWD_FINE: 

					MovePosRelative(&Mach , &gAxis[i] , Mach.pCurCfg->FineDist , Mach.pCurCfg->IncrVel , Mach.pCurCfg->IncrAcc);
					Mach.Cmd.PrintCylind[k] = 0;

				break;	

				case CMD_FWD_COARSE:  

					MovePosRelative(&Mach ,  &gAxis[i] , Mach.pCurCfg->CoarseDist , Mach.pCurCfg->IncrVel , Mach.pCurCfg->IncrAcc);
					Mach.Cmd.PrintCylind[k] = 0;

				break;	

				case CMD_REV_FINE:  

					MoveNegRelative(&Mach ,  &gAxis[i] , Mach.pCurCfg->FineDist , Mach.pCurCfg->IncrVel , Mach.pCurCfg->IncrAcc);
					Mach.Cmd.PrintCylind[k] = 0;

				break;	

				case CMD_REV_COARSE:  

					MoveNegRelative(&Mach ,  &gAxis[i] , Mach.pCurCfg->CoarseDist , Mach.pCurCfg->IncrVel , Mach.pCurCfg->IncrAcc);
					Mach.Cmd.PrintCylind[k] = 0;

				break;	

				
				case CMD_JOG_POSITIVE:

					//mAXIS[i].Cmd.JogFWD = 1;
					//Mach.Cmd.PrintCylind[k] = 0;
					
				break;	
				

				case CMD_ACCEPT_POS:

					gAxis[i].msc.pCurCfg->start_offset =( (gAxis[i].Status.MASTER_val%FULL_ROT) -(gAxis[i].Status.ActPosition%FULL_ROT) + FULL_ROT)%FULL_ROT;
					Mach.Cmd.PrintCylind[k] = 0;
					
				break;

				default:

					Mach.Cmd.PrintCylind[k] = 0;
					

				break;	

			}

			///////////////////////////////////

			mAXIS[i].Cmd.JogFWD = (Mach.Cmd.PrintCylind[k] == CMD_JOG_POSITIVE) || Ilum_Buttons[0].Input[0]  ;
			
			
		}


		//  ON AUTO OFF
		
		switch(Mach.pCurCfg->PrintCylind_Mode[k]){


			case OFF:

				mAXIS[i].Cmd.Active = 0;

			break;

			case ON:

				mAXIS[i].Cmd.Active = 1;

				if(Mach.shft_step > 0)
					mAXIS[i].Cmd.JogFWD = 0;
				

			break;	
			
			case AUTO:

				mAXIS[i].Cmd.Active = 1;

				if(Mach.shft_step > 0){

					mAXIS[i].Cmd.JogFWD = 0;
					
					if((mAXIS[i].Status.Status != STAT_SYNCH) 
						|| (mAXIS[i].Status.Status != STAT_RESYN))
						mAXIS[i].Cmd.Resynchro = 1;

				}				

			break;

		}

		// Status		
		Mach.Status.PrintCylind[k] = mAXIS[i].Status.Status;
		
	}	

	///////   ANILOX Roller   /////////////////////////

	for ( i = ANILOX_1; i <= ANILOX_5; i++)	{

		k = i -ANILOX_1;

		if(!Mach.pCurCfg->Color_Active[k])
			break;

		vaxis = (ACP10AXIS_typ*) gAxis[i].msc.pVaxis;
		//  Relative Movement - FWD & REV	

		if((Mach.pCurCfg->PrintCylind_Mode[k] == OFF)){

			mAXIS[i].Cmd.Active = 0;

			if(gAxis[i].Status.MoveMode == ncACTIVE){

				if(gAxis[i].Status.VaxMoveMode != ncOFF)
						gAxis[i].Command.VaxStop = 1;

			}

		}
		else {

			if(gAxis[i].msc.step != 0){
				
				
				//  ON AUTO OFF
				
				switch(Mach.pCurCfg->AniloxRoller_Mode[k]){


					case OFF:

						mAXIS[i].Cmd.Active = 0;

						if(gAxis[i].Status.MoveMode == ncACTIVE){

							if(gAxis[i].Status.VaxMoveMode != ncOFF)
									gAxis[i].Command.VaxStop = 1;

						}


					break;

					case ON:

						mAXIS[i].Cmd.Active = 1;

						if(gAxis[i].Status.MoveMode == ncACTIVE){

							if(gAxis[i].Status.VaxMoveMode != ncOFF)
									gAxis[i].Command.VaxStop = 1;

						}

					break;

					
					case AUTO:

						mAXIS[i].Cmd.Active = 1;


						if(gAxis[i].Status.MoveMode == ncACTIVE){
						
							if(Mach.Cmd.AniloxRun){

								if(gAxis[i].Status.VaxMoveMode == ncOFF){

									vaxis->move.basis.parameter.v_pos = (Mach.pCurCfg->AniloxVel / 60.0) * FULL_ROT;
									vaxis->move.basis.parameter.a1_pos = Mach.pCurCfg->AniloxAcc;
									vaxis->move.basis.parameter.a2_pos = vaxis->move.basis.parameter.a1_pos;
															
									gAxis[i].Command.VaxMoveVelocity = 1;
								
								}
							}
							else {

								if(gAxis[i].Status.VaxMoveMode != ncOFF)
									gAxis[i].Command.VaxStop = 1;

							}	

						}

					break;

				}			

			}

		}

		// Status		
		Mach.Status.AniloxRoller[k] = mAXIS[i].Status.Status;

	}

	/////////   DR BLADE  ////////////////////////////

	for ( i = ANILOX_1; i <= ANILOX_5; i++)	{

		k = i -ANILOX_1;

		switch(Mach.pCurCfg->AniloxRoller_Mode[k]){


			case OFF:				


			break;

			case ON:
				

			break;

			
			case AUTO:


			break;	

		}	

	}	

	/////////  INK PUMP  ////////////////////////////

	for ( i = ANILOX_1; i <= ANILOX_5; i++)	{

		k = i -ANILOX_1;

		switch(Mach.pCurCfg->AniloxRoller_Mode[k]){


			case OFF:				


			break;

			case ON:
				

			break;

			
			case AUTO:


			break;	

		}

	}	

	//////////  INTERFACE to MAIN MACHINE/////////////	

	if (Mach.Alarm || !Mach.ESTOP){

		// Set ESTOP contact to main Machine 		

	}	

	///////
	ready = 1;

	for ( i = IMPR_CYLIND ; i <= INSIDE_CYLIND; i++)	{	

		if(!((mAXIS[i].Status.Status == STAT_SYNCH)
				|| (mAXIS[i].Status.Status == STAT_PAUSE)))
				ready = 0;
	}	
	
	for ( i = 0; i < COLOR_NUM; i++)	{		

		if(Mach.pCurCfg->Color_Active[i]){

			if(!((mAXIS[i+COLOR_1].Status.Status == STAT_SYNCH)
				|| (mAXIS[i+COLOR_1].Status.Status == STAT_PAUSE)))
				ready = 0;

			if(!((mAXIS[i+ANILOX_1].Status.Status == STAT_SYNCH)
				|| (mAXIS[i+ANILOX_1].Status.Status == STAT_PAUSE)))
				ready = 0;	

		}	

	}
	  // ready  -- RUN Full speed allowed

	  ///////
	readyJog = 1;

	for ( i = IMPR_CYLIND ; i <= INSIDE_CYLIND; i++)	{	

		if(!((mAXIS[i].Status.Status == STAT_OFF)
				|| (mAXIS[i].Status.Status == STAT_PAUSE)))
				ready = 0;
	}	
	
	for ( i = 0; i < COLOR_NUM; i++)	{		

		if(Mach.pCurCfg->Color_Active[i]){

			if(!((mAXIS[i+COLOR_1].Status.Status == STAT_OFF)
				|| (mAXIS[i+COLOR_1].Status.Status == STAT_ERROR)))
				readyJog = 0;

			if(!((mAXIS[i+ANILOX_1].Status.Status == STAT_OFF)
				|| (mAXIS[i+ANILOX_1].Status.Status == STAT_ERROR)))
				readyJog = 0;	

		}	

	}
	  // readyJog  -- Jog to synchronize speed allowed


	//////////////////////////////////////////////


	for(i = 0 ; i < 2 ; i++)
		Ilum_Btn_srv(&Ilum_Buttons[i]);
	
}

void _EXIT ProgramExit(void)
{
	

}


void CFG_HardCode(){	
int i;

	
	///////////////////////

	init_Status[IMPR_CYLIND] = ACPx_Initial(&gAxis[IMPR_CYLIND],		&gAx_CenImp ,	& gVx_CenImp , 	"ErrorText" , 1 , &ServoCfg[IMPR_CYLIND] , &ServoCurCfg[IMPR_CYLIND]);	
	init_Status[INSIDE_CYLIND] = ACPx_Initial(&gAxis[INSIDE_CYLIND],	&gAx_Inside , 	&gVx_Inside ,	 "ErrorText" , 2 , &ServoCfg[INSIDE_CYLIND] , &ServoCurCfg[INSIDE_CYLIND]);

	init_Status[ANILOX_1] = ACPx_Initial(&gAxis[ANILOX_1],	 &gAx_Ink1  , 	&gVx_Ink1 ,	"ErrorText" , 1 , &ServoCfg[ANILOX_1] , &ServoCurCfg[ANILOX_1]);
	init_Status[COLOR_1] = ACPx_Initial(&gAxis[COLOR_1],	&gAx_Col1  , 	&gVx_Col1 , 	"ErrorText" , 1 , &ServoCfg[COLOR_1] , &ServoCurCfg[COLOR_1]);	

	init_Status[ANILOX_2] = ACPx_Initial(&gAxis[ANILOX_2],	&gAx_Ink2  ,		&gVx_Ink2 , 	"ErrorText" , 2 , &ServoCfg[ANILOX_2] , &ServoCurCfg[ANILOX_2]);
	init_Status[COLOR_2] = ACPx_Initial(&gAxis[COLOR_2],	&gAx_Col2  ,		&gVx_Col2 , 	"ErrorText" , 2 , &ServoCfg[COLOR_2] , &ServoCurCfg[COLOR_2]);	

	init_Status[ANILOX_3] = ACPx_Initial(&gAxis[ANILOX_3],	&gAx_Ink3  ,		&gVx_Ink3 ,	"ErrorText" , 1 , &ServoCfg[ANILOX_3] , &ServoCurCfg[ANILOX_3]);
	init_Status[COLOR_3] = ACPx_Initial(&gAxis[COLOR_3],	&gAx_Col3  ,		&gVx_Col3 , 	"ErrorText" , 1 , &ServoCfg[COLOR_3] , &ServoCurCfg[COLOR_3]);	

	init_Status[ANILOX_4] = ACPx_Initial(&gAxis[ANILOX_4],	&gAx_Ink4  ,		&gVx_Ink4 ,	"ErrorText" , 2 , &ServoCfg[ANILOX_4] , &ServoCurCfg[ANILOX_4]);
	init_Status[COLOR_4] = ACPx_Initial(&gAxis[COLOR_4],	&gAx_Col4  ,		&gVx_Col4 , 	"ErrorText" , 2 , &ServoCfg[COLOR_4] , &ServoCurCfg[COLOR_4]);

	init_Status[ANILOX_5] = ACPx_Initial(&gAxis[ANILOX_5],	&gAx_Ink5  ,		&gVx_Ink5 , 	"ErrorText" , 1 , &ServoCfg[ANILOX_5] , &ServoCurCfg[ANILOX_5]);
	init_Status[COLOR_5] = ACPx_Initial(&gAxis[COLOR_5],	&gAx_Col5  ,		&gVx_Col5 , 	"ErrorText" , 2 , &ServoCfg[COLOR_5] , &ServoCurCfg[COLOR_5]);

	init_Status[M_SHAFT] = ACPx_Initial(&gAxis[M_SHAFT],	&gAx_Pwr1  ,		&gVx_Pwr1 , 	"ErrorText" , 2 , &ServoCfg[M_SHAFT] , &ServoCurCfg[M_SHAFT]);	

	

/************SERVO PARAMS **********/
/*************************************/

for( i = COLOR_1 ; i <= COLOR_5 ; i++){


	mAXIS[i].Param.COMP_MA_S = FULL_ROT;
	mAXIS[i].Param.COMP_SL_S  = FULL_ROT;

	ServoCfg[i].units = FULL_ROT;
	ServoCfg[i].rev_motor = 4;
	ServoCfg[i].count_dir = ncSTANDARD;
	ServoCfg[i].unit_precision = FULL_ROT;

	ServoCfg[i].home_mode = ncHOME_OFFSET; //ncDIRECT

	/* In acp Units */
	ServoCfg[i].pos_sw_end = 0x7FFFFFFF;
	ServoCfg[i].neg_sw_end = 0x80000001;

	/* In acp Units */
	ServoCfg[i].a_max = FULL_ROT * 500.0 ;
	ServoCfg[i].v_max = FULL_ROT * 100.0 ;

	ServoCfg[i].lagErrorMax = FULL_ROT;

	if(Mach.pCurCfg->Mode)
		strcpy(ServoCfg[i].ParTableName[0], "AUT_ENC");
	else
		strcpy(ServoCfg[i].ParTableName[0], "AUT_STND");
	
	ServoCfg[i].ParTableName[1][0] = 0;	
	ServoCfg[i].CamName[0][0] = 0;

}
	
/////////////////////////////////////////////////////////////

for( i = ANILOX_1 ; i <= ANILOX_5 ; i++){


	mAXIS[i].Param.COMP_MA_S = FULL_ROT;
	
	ServoCfg[i].units = FULL_ROT;
	ServoCfg[i].rev_motor = 2;
	ServoCfg[i].count_dir = ncINVERSE;
	ServoCfg[i].unit_precision = FULL_ROT/(102.13 * M_PI);
;

	//ServoCfg[i].unit_precision =  FULL_ROT /300.0 * M_PI / mm per full turn / FULL_ROT

	ServoCfg[i].home_mode = ncHOME_OFFSET; //ncDIRECT

	/* In acp Units */
	ServoCfg[i].pos_sw_end = 0x7FFFFFFF;
	ServoCfg[i].neg_sw_end = 0x80000001;

	/* In acp Units */
	ServoCfg[i].a_max = FULL_ROT * 500.0 ;
	ServoCfg[i].v_max = FULL_ROT * 50.0 ;

	ServoCfg[i].lagErrorMax = FULL_ROT;


	if(Mach.pCurCfg->Mode)		
		strcpy(ServoCfg[i].ParTableName[0], "AUT_ENC");
	else
		strcpy(ServoCfg[i].ParTableName[0], "AUT_STND");
	
	ServoCfg[i].ParTableName[1][0] = 0;	
	ServoCfg[i].CamName[0][0] = 0;

}

	///////////////////////////////////////////////
	
	mAXIS[INSIDE_CYLIND].Param.COMP_MA_S = FULL_ROT;
	
	ServoCfg[INSIDE_CYLIND].units = FULL_ROT;
	ServoCfg[INSIDE_CYLIND].rev_motor = 4;
	ServoCfg[INSIDE_CYLIND].count_dir = ncSTANDARD;
	ServoCfg[INSIDE_CYLIND].unit_precision = FULL_ROT / (199.97 * M_PI); 

	//ServoCfg[INSIDE_CYLIND].unit_precision =  FULL_ROT /300.0 * M_PI / mm per full turn / FULL_ROT

	ServoCfg[INSIDE_CYLIND].home_mode = ncHOME_OFFSET;  //ncDIRECT

	/* In acp Units */
	ServoCfg[INSIDE_CYLIND].pos_sw_end = 0x7FFFFFFF;
	ServoCfg[INSIDE_CYLIND].neg_sw_end = 0x80000001;

	/* In acp Units */
	ServoCfg[INSIDE_CYLIND].a_max = FULL_ROT * 500.0 ;
	ServoCfg[INSIDE_CYLIND].v_max = FULL_ROT * 50.0 ;

	ServoCfg[INSIDE_CYLIND].lagErrorMax = FULL_ROT * 10;


	if(Mach.pCurCfg->Mode)		
		strcpy(ServoCfg[INSIDE_CYLIND].ParTableName[0], "AUT_ENC");
	else
		strcpy(ServoCfg[INSIDE_CYLIND].ParTableName[0], "AUT_STND_V");
		
	ServoCfg[INSIDE_CYLIND].ParTableName[1][0] = 0;	
	ServoCfg[INSIDE_CYLIND].CamName[0][0] = 0;


	///////////////////////////////////////////////////

	mAXIS[IMPR_CYLIND].Param.COMP_MA_S = FULL_ROT;
	
	ServoCfg[IMPR_CYLIND].units = FULL_ROT;
	ServoCfg[IMPR_CYLIND].rev_motor = 4;
	ServoCfg[IMPR_CYLIND].count_dir = ncINVERSE;
	ServoCfg[IMPR_CYLIND].unit_precision = FULL_ROT /(600.476 * M_PI);

	//ServoCfg[IMPR_CYLIND].unit_precision =  FULL_ROT /300.0 * M_PI / mm per full turn / FULL_ROT

	ServoCfg[IMPR_CYLIND].home_mode = ncHOME_OFFSET;

	/* In acp Units */
	ServoCfg[IMPR_CYLIND].pos_sw_end = 0x7FFFFFFF;
	ServoCfg[IMPR_CYLIND].neg_sw_end = 0x80000001;

	/* In acp Units */
	ServoCfg[IMPR_CYLIND].a_max = FULL_ROT * 500.0 ;
	ServoCfg[IMPR_CYLIND].v_max = FULL_ROT * 50.0 ;

	ServoCfg[IMPR_CYLIND].lagErrorMax = FULL_ROT * 10.0;


	if(Mach.pCurCfg->Mode)		
		strcpy(ServoCfg[IMPR_CYLIND].ParTableName[0], "AUT_ENC");
	else
		strcpy(ServoCfg[IMPR_CYLIND].ParTableName[0], "AUT_STND");
	
	ServoCfg[IMPR_CYLIND].ParTableName[1][0] = 0;	
	ServoCfg[IMPR_CYLIND].CamName[0][0] = 0;


	////////////////////////////////////////////
	if(Mach.pCurCfg->Color5Dir){
		ServoCfg[INSIDE_CYLIND].count_dir 	= ncSTANDARD;
		ServoCfg[ANILOX_5].count_dir 		= ncSTANDARD;
		ServoCfg[COLOR_5].count_dir 		= ncINVERSE;
		
	}else{
	
		ServoCfg[INSIDE_CYLIND].count_dir 	= ncINVERSE;
		ServoCfg[ANILOX_5].count_dir 		= ncINVERSE;
		ServoCfg[COLOR_5].count_dir 		= ncSTANDARD;
	}

	///////////////////////////////////////////////

	mAXIS[M_SHAFT].Param.COMP_MA_S = FULL_ROT;
	
	ServoCfg[M_SHAFT].units = FULL_ROT;
	ServoCfg[M_SHAFT].rev_motor = 1;
	ServoCfg[M_SHAFT].count_dir = ncSTANDARD;
	ServoCfg[M_SHAFT].unit_precision = FULL_ROT;

	//ServoCfg[IMPR_CYLIND].unit_precision =  FULL_ROT /300.0 * M_PI / mm per full turn / FULL_ROT

	ServoCfg[M_SHAFT].home_mode = ncHOME_OFFSET;

	/* In acp Units */
	ServoCfg[M_SHAFT].pos_sw_end = 0x7FFFFFFF;
	ServoCfg[M_SHAFT].neg_sw_end = 0x80000001;

	/* In acp Units */
	ServoCfg[M_SHAFT].a_max = FULL_ROT * 25.0 ;
	ServoCfg[M_SHAFT].v_max = FULL_ROT * 25.0 ;

	ServoCfg[M_SHAFT].lagErrorMax = FULL_ROT;


	strcpy(ServoCfg[M_SHAFT].ParTableName[0], "AUT_MS_E");	 // AUT_MS_V   AUT_MS_E
	ServoCfg[M_SHAFT].ParTableName[1][0] = 0;	
	ServoCfg[M_SHAFT].CamName[0][0] = 0;


	/////////////////////////////////////////////

	CfgReady = 1;

}

void MovePosRelative(MACH_obj *m , ACP_typ *ax , float x , float v , float a){
ACP10VAXIS_typ *vaxis;
	

	if(m->pCurCfg->BlankLen > 0.0){
		
		vaxis = (ACP10AXIS_typ*) ax->msc.pVaxis;
		
		if(vaxis){

			if(vaxis->move.mode == ncOFF){
		
				vaxis->move.basis.parameter.s = (DINT) ((x/m->pCurCfg->BlankLen) * FULL_ROT);
				vaxis->move.basis.parameter.v_pos =  (v/m->pCurCfg->BlankLen) * FULL_ROT;
				vaxis->move.basis.parameter.a1_pos = a;
				vaxis->move.basis.parameter.a2_pos = vaxis->move.basis.parameter.a1_pos;
				
				ax->msc.pCurCfg->start_offset +=   vaxis->move.basis.parameter.s;
				
				ax->msc.pCurCfg->start_offset =  (ax->msc.pCurCfg->start_offset + FULL_ROT)%FULL_ROT;
							
				ax->Command.VaxMoveAdditive = 1;	

			}

		}

	}

	return;

}

void MoveNegRelative(MACH_obj *m , ACP_typ *ax , float x , float v , float a){
ACP10VAXIS_typ *vaxis;


	if(m->pCurCfg->BlankLen > 0.0){

		vaxis = (ACP10AXIS_typ*) ax->msc.pVaxis;
		
		if(vaxis){

			if(vaxis->move.mode == ncOFF){
		
				vaxis->move.basis.parameter.s = (DINT) ((x/m->pCurCfg->BlankLen) * FULL_ROT) * (-1);
				vaxis->move.basis.parameter.v_neg =  (v/m->pCurCfg->BlankLen) * FULL_ROT;
				vaxis->move.basis.parameter.a1_neg = a;
				vaxis->move.basis.parameter.a2_pos = vaxis->move.basis.parameter.a1_neg;

				ax->msc.pCurCfg->start_offset +=   vaxis->move.basis.parameter.s;
				
				ax->msc.pCurCfg->start_offset =  (ax->msc.pCurCfg->start_offset + FULL_ROT)%FULL_ROT;
							
				ax->Command.VaxMoveAdditive = 1;	

			}


		}

	}

	return;

}

void Ilum_Btn_srv(ILUM_BTN_obj *btn){
int i;

	if(! btn)
		return;

	for(i = 0 ; i < 6 ; i++)
		btn->_Input[i] = btn->Input[i];

	for( i = 0 ; i < 24 ; i++)
		btn->Out_Bits[i] = 0;
		

	for ( i =0 ; i < 6 ; i++){

		if(btn->Out_Color[i] > 0){			
			if(btn->Out_Color[i] < 5)
				btn->Out_Bits[i*4 + btn->Out_Color[i]-1]	= 1;	
		}
		
	}	

	return;
	
}

/*///  Data from Curtius
CI cylinder (Drum) diameter=600.476mm
Anilox roller diameter=102.13mm
Plate cylinder diameter =60.600mm 
Drawings attached.
 
Outside diameter of sleeve cylinder is 68.5 mm
Diameter with printing plate =72.5 mm
Sticky bag + printing plate is =2 mm
 
Gearboxes:
 Five KH090 - 004 / B&R 8LSA44-ee030ff00-0 (19mm Shaft)
Ratio = 4:1
KH110 - 004/ B&R 8LSA55-ee030ff00-0   (24mm shaft)   1 EACH
Ratio: 4:1
KH140 - 004 / B&R 8LSA57-ee030ff00-0   (24mm shaft)  1 EACH
Ratio:  4:1
 
Timing Pulley on Anilox roller 32T.
Timing Pulley on Anilox Servomotor 16T
*/
