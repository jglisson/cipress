/*********************************************************
* FILE    	: 		timer_lib.c
*
* OBJECT	: 	N/A
*
* OVERVIEW	: 	Common Prototypes and Dependancies
*
*
* HISTORY : 		02.20.06 - Version 1
*
*
*
*
* DEPENDANCIES:
*********************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif
#include "ACPx.h"
#ifdef __cplusplus
	};
#endif

#include "timer_lib.h"


/********************************************************
* CODE BASE:
*********************************************************/
SINT HSReset( HS_obj *hs )
{
	memset( hs, 0, sizeof(HS_obj ));
	return 0;
}

BOOL HSCompare( HS_obj *hs, DINT status, DINT compare )
{
	/* Proper Execution: Upstream Process */
	if ( status == compare ) {
		hs->Ready	= 1;
		hs->Error	= 0;
		return 1;
	}
	else
		return 0;
}


DINT TimerStart( TON_typ *timer, REAL timeoutTime ) {
	timer->IN = 0;
	TON(timer);
	timer->IN = 1;
	timer->PT = (int) ((timeoutTime * 1000.0)+0.5);
	TON(timer);
	return 1;
}

BOOL TimerExecute( TON_typ *timer ) {
	TON(timer);
	return 1;
}

REAL TimerTimeRemaining( TON_typ *timer ) {
	return  ( (REAL) (timer->PT - timer->ET) /1000.0);
}

BOOL TimerIsTimeOut( TON_typ *timer ) {
	return timer->Q;
}

DINT sm_init( SM_obj *sm  )
{

	/* Text Field: */
	strcpy( sm->Text, "Yet to be Started" );

	/* Default */
	sm->oldStep			= 255;
	sm->Step			= 0;
	return 1;
}

DINT sm_serv( SM_obj *sm )
{
	/* Step Transition */
	if (sm->Step != sm->oldStep) {
		sm->new		= ON;
	    	sm->oldStep 		= sm->Step;
	}

	return 1;
}

DINT sm_text( STRING text[32] , SM_obj *sm)
{
USINT stringh[50];
USINT step[4];

	/* Bad Pointer or No New Data */
	if ((sm == 0) || (!sm->new) )
		return 0;

	/* Turn Off Signal */
	sm->new = OFF;

	/* String Prefix */
	itoa(sm->Step, (UDINT) step );
	strcpy( stringh, "Step " );
	strcat( stringh, step );
	strcat( stringh, ": " );
	/* Check String Length */
	if ( strlen( text ) > 32 )
		strcat( stringh, "Text too Long" );
	else
		/* Last Piece */
		strcat( stringh, text );

	/* Output */
	strcpy( sm->Text, stringh );
	return 1;

}

UINT BitToWord( BOOL *b )
{
UINT i, wordh = 0;

	for (i=0;i<16;i++,b++)
		wordh = wordh + ((UINT) *b << i);

	return wordh;
}

/* Return: Quantity of High Bits */
USINT WordToBit( DINT x, BOOL *b)
{
UINT i;
USINT q = 0;

	/* Assign Masks */
	for (i=0;i<16;i++,b++) {
		*b = (BOOL) ( x >> i ) & 0x01;
		if (*b)
			q++;
	}

	return q;
}


UINT SetBit( UINT wordh, UINT Bit_Num )
{
UINT offset = 1;

	offset <<= Bit_Num;

	wordh |= (UINT) offset;

	return wordh;
}

UINT ResetBit( UINT wordh, UINT Bit_Num )
{
UINT inverse = 0xFFFF;
UINT offset = 1;

	offset <<= Bit_Num;

	inverse ^= offset;

	wordh &= inverse;
	return wordh;
}

USINT GetHighBit( UINT wordh )
{
UINT Answer;
USINT i;

	for (i=0;i<16;i++) {
		Answer = (wordh >> i) & 0x0001;
		if (Answer > 0)
			return i;
	}
	return 0;
}

USINT GetNibble( USINT x, BOOL ho )
{
	if (ho) 	return ((x>>4)	& 0x0F);
	else		return ((x>>0)	& 0x0F);
}

USINT NibblesToByte(USINT o1, USINT o2)
{
	return (((o2<<4)  & 0xF0) | ((o1 & 0x0F)));
}
/* EOF */


