/*********************************************************
* FILE    	: 		timer_lib.c
*
* OBJECT	: 	N/A
*
* OVERVIEW	: 	Common Prototypes and Dependancies
*
*
* HISTORY : 		02.20.06 - Version 1
*
*
*
*
* DEPENDANCIES:
*********************************************************/
#include <bur/plc.h>
#include <bur/plctypes.h>
#include <astime.h>
#include <string.h>
#include <standard.h>
#include <StateLib.h>

/********************************************************
* BLOCK POINTERS:
*********************************************************/
TON_typ *t1,*t2,*t3;
SM_obj *sm;

/********************************************************
* DEFINITIONS:
*********************************************************/

/* General */
#define ON							1
#define OFF							0
#define YES							1
#define NO							0

/* State Machine */
#define BASE_SM						0
#define BASE_SM2					6
#define ERROR_SM					100
#define BEGIN_SM 		sm_serv( sm );TimerExecute(t1);TimerExecute(t2);TimerExecute(t3);switch( sm->Step ) {
#define END_SM   		default:;  TB "Invalid Step " TE;break;}
#define INC_SM 			sm->Step++
#define GO_BASE_SM		sm->Step = BASE_SM
#define GO_BASE_SM2	sm->Step = BASE_SM2
#define GOTO_SM			sm->Step =
#define RESET_SM		sm->Step = 0
#define GO_ERROR_SM	sm->ErrorStep	= sm->Step;sm->Step	= ERROR_SM

/* Timer */
#define IF_TMR1 if ( TimerIsTimeOut( t1 ) )
#define IF_TMR2 if ( TimerIsTimeOut( t2 ) )
#define IF_TMR3 if ( TimerIsTimeOut( t3 ) )
#define TB sm_text(
#define TE , sm);
#define TMR1S TimerStart( t1,
#define TMRE );
#define T2S TimerStart( t2,


#define TMR1_20s 	TimerStart( t1, 20.0 )
#define TMR1_15s 	TimerStart( t1, 15.0 )
#define TMR1_10s 	TimerStart( t1, 10.0 )
#define TMR1_5s 		TimerStart( t1, 5.0 )
#define TMR1_2s 		TimerStart( t1, 2.0 )
#define TMR1_1s 		TimerStart( t1, 1.0 )
#define TMR1_500ms	TimerStart( t1, 0.5 )
#define TMR1_250ms	TimerStart( t1, 0.25 )
#define TMR1_100ms	TimerStart( t1, 0.1 )

#define TMR2_20s 	TimerStart( t2, 20.0 )
#define TMR2_15s 	TimerStart( t2, 15.0 )
#define TMR2_10s 	TimerStart( t2, 10.0 )
#define TMR2_5s 		TimerStart( t2, 5.0 )
#define TMR2_2s 		TimerStart( t2, 2.0 )
#define TMR2_1s 		TimerStart( t2, 1.0 )
#define TMR2_500ms	TimerStart( t2, 0.5 )
#define TMR2_250ms	TimerStart( t2, 0.25 )
#define TMR2_100ms	TimerStart( t2, 0.1 )

#define TMR3_20s 	TimerStart( t3, 20.0 )
#define TMR3_15s 	TimerStart( t3, 15.0 )
#define TMR3_10s 	TimerStart( t3, 10.0 )
#define TMR3_5s 		TimerStart( t3, 5.0 )
#define TMR3_2s 		TimerStart( t3, 2.0 )
#define TMR3_1s 		TimerStart( t3, 1.0 )
#define TMR3_500ms	TimerStart( t3, 0.5 )
#define TMR3_250ms	TimerStart( t3, 0.25 )
#define TMR3_100ms	TimerStart( t3, 0.1 )
/* EOF */


