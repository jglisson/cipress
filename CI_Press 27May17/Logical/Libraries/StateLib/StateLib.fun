FUNCTION HSCompare : BOOL 
VAR_INPUT
		hs	:HS_obj;	
		status	:DINT;	
		compare	:DINT;	
	END_VAR
END_FUNCTION
FUNCTION TimerIsTimeOut : BOOL 
VAR_INPUT
		timer	:TON;	
	END_VAR
END_FUNCTION
FUNCTION TimerExecute : BOOL 
VAR_INPUT
		timer	:TON;	
	END_VAR
END_FUNCTION
FUNCTION TimerStart : DINT 
VAR_INPUT
		timer	:TON;	
		timeoutTime	:REAL;	
	END_VAR
END_FUNCTION
FUNCTION sm_text : DINT 
VAR_INPUT
		text	:STRING[32];	
		sm	:SM_obj;	
	END_VAR
END_FUNCTION
FUNCTION sm_serv : DINT 
VAR_INPUT
		sm	:SM_obj;	
	END_VAR
END_FUNCTION
FUNCTION sm_init : DINT 
VAR_INPUT
		sm	:SM_obj;	
	END_VAR
END_FUNCTION
FUNCTION HSReset : SINT 
VAR_INPUT
		hs	:HS_obj;	
	END_VAR
END_FUNCTION
