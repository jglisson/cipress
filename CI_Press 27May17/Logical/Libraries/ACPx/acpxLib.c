/********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Library: ACPx
 * File: acpxLib.c
 * Author: KK
 * Created: September 02, 2011
 *******************************************************************/

#include <bur/plctypes.h>

#ifdef __cplusplus
	extern "C"
	{
#endif
#include "ACPx.h"
#ifdef __cplusplus
	};
#endif


	typedef struct { /* Data structure for cyclic channel */
	USINT cnt;
	USINT reserve;
	USINT data[16];
	} ncCYCUSDAT_typ;

	signed long ACPx_Initial(struct ACP_typ* a, struct ACP10AXIS_typ* pAxis, struct ACP10VAXIS_typ* pVaxis, unsigned char* ErrorTableName, unsigned short channel, struct ACP_Cfg_typ* pCfg, struct ACP_CurCfg_typ* pCurCfg){

		if(!a)
			return -1;	
		
		a->msc.pAxis = 0;
		a->msc.pVaxis = 0;

		a->msc.pCfg 	= pCfg;
		a->msc.pCurCfg	= pCurCfg;				
		
		a->msc.channel  = channel;
		if(a->msc.channel == 0 || a->msc.channel > 2)
			a->msc.channel == 1;

		

		if(!pAxis)
			return -2;

		a->msc.pAxis = (UDINT)pAxis;

		
		if(pVaxis)
			a->msc.pVaxis = (UDINT)pVaxis;		
		
		// 
		if(ErrorTableName){
			strcpy(pAxis->message.text.parameter.data_modul , ErrorTableName);
			pAxis->message.text.parameter.format   	= ncBREAK;
			pAxis->message.text.parameter.columns	= 81;
			pAxis->message.text.parameter.data_len	= sizeof(a->Status.ErrorText); /* four lines */
			pAxis->message.text.parameter.data_adr	= (UDINT)&a->Status.ErrorText;

			strcpy( a->Status.ErrorText.line1, "No Alarms" );
			strcpy( a->Status.ErrorText.line2, "Servo Okay");
			a->Status.ErrorText.line3[0] = 0;
			a->Status.ErrorText.line4[0] = 0;		
		}

		
		return 1;
		
	}



	signed long ACPx_Init(struct ACP_typ* a, unsigned char* AxisName, unsigned char* VirtualAxisName, unsigned char* ErrorTableName ,UINT channel ,ACP_Cfg_typ* pCfg, ACP_CurCfg_typ* pCurCfg){
		UINT status;
		ACP10AXIS_typ *p_ax_dat = 0;
		ACP10VAXIS_typ *p_vax_dat = 0;
		UDINT ax_obj, vax_obj;

		a->msc.pCfg 	= pCfg;
		a->msc.pCurCfg 	= pCurCfg;
		a->msc.channel  = channel;

		a->msc.pAxis = 0;
		a->msc.pVaxis = 0;

		if(a->msc.channel == 0 || a->msc.channel > 2)
			a->msc.channel == 1;

		if(!AxisName)
			return 0;

			
		
		status = ncaccess(ncACP10MAN ,AxisName,(void*)&ax_obj);

		if ( ( status != ncOK ) && ( status != 10600 ) )
		{
		/* NC Object Pointer ist invalid */
			a->Status.ErrorID = -10;
			return -10;
		}

		if(!ax_obj)
			return -11;
		
		p_ax_dat = (ACP10AXIS_typ*)ax_obj;

	
		if ( p_ax_dat->size != sizeof(ACP10AXIS_typ) )
		{
		/* The NC data type is not compatible with NC manager */
			a->Status.ErrorID = -12;			
			return -12;
		}


		if ( (p_ax_dat->sw_version.nc_manager&0xFFF0)!=(ACP10MAN_H_VERSION&0xFFF0) )
		{
		/* The version of "acp10man.h" is not compatible with NC manager */
			a->Status.ErrorID  = -13;
			return -13;
		}
	
			
		if(VirtualAxisName) {
		
			status = ncaccess(ncACP10MAN,VirtualAxisName,(void*)&vax_obj);
			 
			if ( ( status != ncOK ) && ( status != 10600 ) )
			{
			/* NC Object Pointer ist invalid */
				a->Status.ErrorID = -20;
				return -20;
			}

			if(!vax_obj)
				return -21;
			
			p_vax_dat = (ACP10VAXIS_typ*)vax_obj;

			if ( p_vax_dat->size != sizeof(ACP10VAXIS_typ) )
			{
			/* The NC data type is not compatible with NC manager */
				a->Status.ErrorID = -22;
				return -22;
			}			
		 
			if ( (p_vax_dat->sw_version.nc_manager&0xFFF0)!=(ACP10MAN_H_VERSION&0xFFF0) )
			{
			/* The version of "acp10man.h" is not compatible with NC manager */
				a->Status.ErrorID = -23;
				return -23;
			}
	

		}
		// 
		if(ErrorTableName){
			strcpy(p_ax_dat->message.text.parameter.data_modul , ErrorTableName);
			p_ax_dat->message.text.parameter.format   	= ncBREAK;
			p_ax_dat->message.text.parameter.columns	= 81;
			p_ax_dat->message.text.parameter.data_len	= sizeof(a->Status.ErrorText); /* four lines */
			p_ax_dat->message.text.parameter.data_adr	= (UDINT)&a->Status.ErrorText;

			strcpy( a->Status.ErrorText.line1, "No Alarms" );
			strcpy( a->Status.ErrorText.line2, "Servo Okay");
			a->Status.ErrorText.line3[0] = 0;
			a->Status.ErrorText.line4[0] = 0;
		}

		a->msc.pAxis = (UDINT)p_ax_dat;
		a->msc.pVaxis = (UDINT)p_vax_dat;

		strcpy(a->Status.AxisName , AxisName);

		return 1;
		
	}

	signed long ACPx_Serv( ACP_typ* a)
	{	
	UINT action_status , ncalloc_Status;
	USINT i , k;
	ACP10AXIS_typ *axis;
	ACP10VAXIS_typ *vaxis;
	ncCYCUSDAT_typ *_CYCUSDATFR;
	ncCYCUSDAT_typ *_CYCUSDATTO;


		if(!a) return -1;
		if(!a->msc.pAxis) return -2;

		axis = (ACP10AXIS_typ*) a->msc.pAxis;

		if(a->msc.pVaxis)
			vaxis = (ACP10VAXIS_typ*) a->msc.pVaxis;
		else
			vaxis = 0;
		
					
			
			/* Execute Command Requests */
		switch (a->xCMD) {
	
			/* Wait Command: Ready for New Command */
			case acpWAIT:
			break;
	
			/* Global Init */
			case acpGLOBAL_INIT:
				action_status = ncaction((UDINT)axis, ncGLOBAL, ncINIT);
					if ( action_status == ncOK )
						a->xCMD = acpWAIT;
			break;

			/* Limit Init */
			case acpLIMITS_INIT:
				action_status = ncaction((UDINT)axis, ncLIMITS, ncINIT);
					if ( action_status == ncOK )
						a->xCMD = acpWAIT;
			break;

			/* Download Parameter Table*/  
			case acpPAR_DOWNLOAD:
				
			if(a->Param.pParTableName && (strlen(a->Param.pParTableName)< 11)){
				strcpy(a->msc.table.parameter.data_modul, a->Param.pParTableName); 
				a->msc.table.parameter.data_adr = 0;
				
				axis->network.service.data_adr		= (UDINT)&a->msc.table;
				action_status = ncaction((UDINT)axis, ncACP_PAR+ncSERVICE,ncDOWNLOAD);
				if ( action_status == ncOK )
						a->xCMD = acpWAIT;

			 }
			 else{
				a->Status.ErrorXcmd = a->xCMD;
				a->xCMD = acpWAIT;
			 }
					
					
			break;

			case acpPAR_DOWNLOAD2:
				if((a->msc.table.status.ok ==ncOK)){
					a->xCMD = acpWAIT;
				}
				else if(a->msc.table.status.error){
					a->Status.ErrorXcmd = a->xCMD;
			     	a->xCMD = acpWAIT;
				}
			
			break;
			
			/* Download CAM */  
			case acpCAM_DOWNLOAD:

				if(a->Param.pParTableName && (strlen(a->Param.pParTableName)< 11)){
					
					strcpy(a->msc.table.parameter.data_modul, a->Param.pParTableName); 
					a->msc.table.parameter.data_adr = 0;
					a->msc.table.parameter.index = a->Param.CamID;
					a->msc.table.parameter.mode = ncWITH_INDEX; 					     
					
					axis->network.service.data_adr	= (UDINT)&a->msc.table;

					action_status = ncaction((UDINT)axis, ncSERVICE+ncCAM_PROF,ncDOWNLOAD);
					if ( action_status == ncOK )
							a->xCMD = acpWAIT;
					 
				}
				 else{
					a->Status.ErrorXcmd = a->xCMD;
					a->xCMD = acpWAIT;
				 }

			break;

			
			/* Download PAR_SEQU */
			case acpAUT_WRITE:

				for(i = 0 ;i < 4 ; i++){
					if(a->Param.Aut.state[i].select){				
						a->Param.Aut.state[i].select=0;
						break;
					}
				}

				//* None state selected
				if( i == 4){
					a->xCMD = acpWAIT;
					break;
				}	

			
				
				a->Param.Aut.cnt++;
				k = 0;

				/** State Index **/
				
				a->msc.par_rec[k].par_id 		= ACP10PAR_AUT_ST_INDEX;
				a->msc.idx[i] 					= (USINT)i+1;
				a->msc.par_rec[k].data_adr		= (UDINT) &a->msc.idx[i] ;
				k++;

				
				/** Compensation Slave **/
				
				a->msc.par_rec[k].par_id 		= ACP10PAR_AUT_COMP_SL_S;
				a->msc.par_rec[k].data_adr	= (UDINT) &a->Param.Aut.state[i].comp_sl_s;
				k++;							


				/** Slave Factor **/
							
				a->msc.par_rec[k].par_id 		= ACP10PAR_AUT_SL_FACTOR;
				a->msc.par_rec[k].data_adr	= (UDINT) &a->Param.Aut.state[i].sl_factor;
				k++;							
										

				/** Compensation Master **/					
										
				a->msc.par_rec[k].par_id 		= ACP10PAR_AUT_COMP_MA_S;
				a->msc.par_rec[k].data_adr	= (UDINT) &a->Param.Aut.state[i].comp_ma_s;
				k++;							


				/** Master Factor **/					
											
				a->msc.par_rec[k].par_id 		= ACP10PAR_AUT_MA_FACTOR;
				a->msc.par_rec[k].data_adr	= (UDINT) &a->Param.Aut.state[i].ma_factor;
				k++;	

				/** Cam Index **/		
				if(	a->Param.Aut.state[i].cam_index > 0){						
					a->msc.par_rec[k].par_id 		= ACP10PAR_AUT_ST_DATA_INDEX;
					a->msc.par_rec[k].data_adr	= (UDINT) &a->Param.Aut.state[i].cam_index;
					k++;	
				}
				

				if(k>1){	

					if(a->msc.channel == 0 || a->msc.channel > 2)
						a->msc.channel =1;
					
					a->msc.table.parameter.index		= (a->Param.Aut.cnt%4)+1 + (5*(a->msc.channel-1));
					a->msc.table.parameter.data_modul[0] = 0;
					a->msc.table.parameter.data_adr		= &a->msc.par_rec[0];
					a->msc.table.parameter.mode			= ncWITH_INDEX;
					a->msc.table.parameter.format		= ncFORMAT_ADR;
					a->msc.table.parameter.data_len		= k * sizeof(ACP10PRADR_typ);
					/************************************/

					axis->network.service.data_adr		= (UDINT)&a->msc.table;	
					a->xCMD = acpAUT_WRITE2;
				}
				else
					a->xCMD = acpAUT_WRITE;
				
			break;

			case acpAUT_WRITE2: 	
				
				action_status = ncaction((UDINT)axis, ncSERVICE + ncPAR_SEQU, ncDOWNLOAD + ncINIT);
				if ( action_status == ncOK )
					a->xCMD = acpAUT_WRITE3;
					
			break;

			case acpAUT_WRITE3: 
				
				if((a->msc.table.status.data_len > 0) && (a->msc.table.status.init == ncTRUE)){
					
					a->Status.ErrorXcmd = acpWAIT;
					a->xCMD 	  = acpAUT_WRITE;
				}
				
				if(a->msc.table.status.error == ncTRUE){					
					a->Status.ErrorXcmd = a->xCMD;
					a->xCMD 	  = acpWAIT;
				}

				if(a->Status.Error)
					a->xCMD 	  = acpWAIT;
				
			break;

			
	
			/* Start Automat */
			case acpAUT_START:
				a->Param.ParID_SetVerify	= 0;
				a->Param.ParID			= ACP10PAR_CMD_AUT_START;
				a->Param.ParID_Data		= ncSTART;
				a->xCMD 		= acpSET_PARID;
			break;
	
			/* Stop Automat */
			case acpAUT_STOP:
				a->Param.ParID_SetVerify	= 0;
				a->Param.ParID			= ACP10PAR_CMD_AUT_START;
				a->Param.ParID_Data		= ncSTOP;
				a->xCMD 		= acpSET_PARID;
			break;
	

	
			/* Set Signal 1 */
			case acpAUT_SIGNAL1:
				a->Param.ParID_SetVerify	= 0;
				a->Param.ParID			= ACP10PAR_AUT_SIGNAL_SET;
				a->Param.ParID_Data		= 1;
				a->xCMD 			= acpSET_PARID;
			break;
	
			/* Reset Signal 1 */
			case acpAUT_RESETSIGNAL1:
				a->Param.ParID_SetVerify	= 0;
				a->Param.ParID			= ACP10PAR_AUT_SIGNAL_RESET;
				a->Param.ParID_Data		= 1;
				a->xCMD 			= acpSET_PARID;
			break;

			/* Set Signal 2 */
			case acpAUT_SIGNAL2:
				a->Param.ParID_SetVerify	= 0;
				a->Param.ParID			= ACP10PAR_AUT_SIGNAL_SET;
				a->Param.ParID_Data		= 2;
				a->xCMD 			= acpSET_PARID;
			break;
	
			/* Reset Signal 2 */
			case acpAUT_RESETSIGNAL2:
				a->Param.ParID_SetVerify	= 0;
				a->Param.ParID			= ACP10PAR_AUT_SIGNAL_RESET;
				a->Param.ParID_Data		= 2;
				a->xCMD 			= acpSET_PARID;
			break;
	
			/* VAX1: Set Signal 1 */
			case acpVAX_AUT_SIGNAL1:
				a->Param.ParID_SetVerify	= 0;
				a->Param.ParID			= ACP10PAR_AUT_SIGNAL_SET_VAX1;
				a->Param.ParID_Data		= 1;
				a->xCMD 			= acpSET_PARID;
			break;
	
			/* VAX1: Reset Signal 1 */
			case acpVAX_AUT_RESETSIGNAL1:
				a->Param.ParID_SetVerify	= 0;
				a->Param.ParID			= ACP10PAR_AUT_SIGNAL_RESET_VAX1;
				a->Param.ParID_Data		= 1;
				a->xCMD 			= acpSET_PARID;
			break;
	
			/* Read Service Channel: Location: a->Param.ParID...Data stored: a->Param.ParID_Data */
			case acpREAD_PARID:
				axis->network.service.request.par_id = a->Param.ParID;
				axis->network.service.data_adr		 = (UDINT)&a->Param.ParID_Data;
				action_status = ncaction((UDINT)axis, ncSERVICE, ncREAD);
				if ( action_status == ncOK ) 
					a->xCMD = acpREAD_PARID2;	
			break;
			
			/* Read Service Channel: Location: a->Param.ParID...Data stored: a->Param.ParID_Data */
			case acpREAD_PARID_REAL:
				axis->network.service.request.par_id = a->Param.ParID;
					axis->network.service.data_adr		 = (UDINT)&a->Param.ParID_Real;
					action_status = ncaction((UDINT)axis, ncSERVICE, ncREAD);
					if ( action_status == ncOK ) 
						a->xCMD = acpREAD_PARID2_REAL;	
			break;
			
			/* Read ParID DINT and REAL: Wait for Response */
			/* SetVerify: If TRUE...verify set values with read values */
			case acpREAD_PARID2:
			case acpREAD_PARID2_REAL:
				if (axis->network.service.response.par_id != 0) {
					/* Operation complete: */
						if ( axis->network.service.response.par_id == axis->network.service.request.par_id ) {
							if ( a->Param.ParID_SetVerify ) {
								if ( a->xCMD == acpREAD_PARID2 ) {
									/* Verified: Complete */
									if ( a->Param.ParID_Data == a->msc._parid_data )
										a->xCMD = acpWAIT;
									/* Verify Failure: Error */ 
									else{
										a->Status.ErrorXcmd = a->xCMD;
										a->xCMD 	= acpWAIT;
									}
								}
								else {
										/* Verified: Complete*/ 
										if ( fabsf(a->Param.ParID_Real- a->msc._parid_real) <= fabsf(a->Param.ParID_Real*0.05))
											a->xCMD = acpWAIT;
										 /*Verify Failure: Error */ 
										else{
											a->Status.ErrorXcmd = a->xCMD;
											a->xCMD 	= acpWAIT;
										}
										
										
								}		
							}
							/* No Verify: Completed */	
							else
								a->xCMD = acpWAIT;
						}
					/* Bad Read: Error */
					else {
						a->Status.ErrorXcmd = a->xCMD;
						a->xCMD 	= acpWAIT;
					}		
				}
			break;
			
	
			/* Set ParID:		DINT */
			case acpSET_PARID:
				axis->network.service.request.par_id	= a->Param.ParID;
				axis->network.service.data_adr			= (UDINT)&a->Param.ParID_Data;
				action_status = ncaction((UDINT)axis, ncSERVICE, ncSET);
				//memset(axis->network.service.data_text , 0 , sizeof(axis->network.service.data_text));			itoa(a->Param.ParID_Data ,axis->network.service.data_text);
				//action_status = ncaction((UDINT)axis, ncSERVICE, ncSET + ncDATA_TEXT);(ax_obj,ncSERVICE,ncSET);
				if ( action_status == ncOK )
					a->xCMD = acpSET_PARID2;
			break;
			
			/* Set ParID_REAL:	REAL */
			case acpSET_PARID_REAL:
				axis->network.service.request.par_id = a->Param.ParID;
				axis->network.service.data_adr		 = (UDINT)&a->Param.ParID_Real;	
					action_status = ncaction((UDINT)axis, ncSERVICE, ncSET);
					if ( action_status == ncOK )
						a->xCMD = acpSET_PARID2_REAL;
			break;
			
			/* Set ParID DINT: Wait for Response then proceed to Read operation with Verify */
			case acpSET_PARID2:
				if (axis->network.service.response.par_id != 0) {
						/* Operation complete: Store Requested Data for Read Operation */
						if ( axis->network.service.response.par_id == axis->network.service.request.par_id ) {
						if ( a->Param.ParID_SetVerify ) {
							a->xCMD 			= acpREAD_PARID;
								a->msc._parid_real = a->Param.ParID_Real;
							}
							else {
							a->xCMD 		= acpWAIT;	
							}
						}
					/* Bad Set: Error */ 
					else {
						a->Status.ErrorXcmd	= a->xCMD;
						a->xCMD 		= acpWAIT;
					}	
				}
			break;
			
			/* Set ParID Real: Wait for Response then proceed to Read operation with Verify */
			case acpSET_PARID2_REAL:
				if (axis->network.service.response.par_id != 0) {
						/* Operation complete: Store Requested Data for Read Operation */
						if ( axis->network.service.response.par_id == axis->network.service.request.par_id ) {
							if ( a->Param.ParID_SetVerify ) {
								a->xCMD 			= acpREAD_PARID_REAL;
									a->msc._parid_real		= a->Param.ParID_Data;
							}
							else {
								a->xCMD 		= acpWAIT;		
							}
						}  
					/* Bad Set: Error */ 
					else {
						a->Status.ErrorXcmd	= a->xCMD;							
						a->xCMD 	= acpWAIT;
					}  
				}
			break;
	
			/* Controller ON */
			case acpCONTROLLER_ON:
				action_status = ncaction((UDINT)axis, ncCONTROLLER, ncSWITCH_ON);
				if ( action_status == ncOK )
					a->xCMD = acpWAIT;
			break;
	
			/* Controller OFF */
			case acpCONTROLLER_OFF:
				action_status = ncaction((UDINT)axis, ncCONTROLLER, ncSWITCH_OFF);
				if ( action_status == ncOK )
					a->xCMD = acpWAIT;
			break;
	
			/* VAX Home */
			case acpVAX_HOME:
				if(vaxis){
					vaxis->move.homing.parameter.s			= 0;
					vaxis->move.homing.parameter.mode		= ncDIRECT;
					action_status = ncaction((UDINT)vaxis,ncHOMING,ncSTART);
					if ( action_status == ncOK )
						a->xCMD = acpWAIT;
				}
				else
					a->xCMD = acpWAIT;
					
			break;	

			/* Relative Move */
			case acpVAX_REL_MOVE:
				
				/* Bad Parameter */
				if ( vaxis->move.basis.parameter.s == 0)
					a->xCMD 		= acpWAIT;
	
				else{					
					action_status = ncaction((UDINT)vaxis, ncREL_MOVE, ncSTART + ncINIT);
					if ( action_status == ncOK )
						a->xCMD = acpWAIT;
						
				}
			break;
	
			case acpHOME_DIRECT:
				axis->move.homing.parameter.s			= a->Param.HomePosition;
				axis->move.homing.parameter.mode		= ncDIRECT;
				action_status = ncaction((UDINT)axis,ncHOMING,ncSTART);
				if ( action_status == ncOK )
					a->xCMD = acpWAIT;
			break;
	
			case acpHOME_OFFSET_CORR:
				axis->move.homing.parameter.s			= a->Param.HomePosition;
				axis->move.homing.parameter.mode		= ncHOME_OFFSET + ncCORRECTION;
				action_status = ncaction((UDINT)axis,ncHOMING,ncSTART);
				if ( action_status == ncOK )
					a->xCMD = acpWAIT;
			break;
	
			case acpHOME_OFFSET:			
				axis->move.homing.parameter.s			= a->Param.HomePosition;
				axis->move.homing.parameter.mode		= ncHOME_OFFSET;
				action_status = ncaction((UDINT)axis,ncHOMING,ncSTART);
				if ( action_status == ncOK )
					a->xCMD = acpWAIT;
			break;	

			case acpHOME:			
				axis->move.homing.parameter.s			= a->Param.HomePosition;				
				action_status = ncaction((UDINT)axis,ncHOMING,ncSTART);
				if ( action_status == ncOK )
					a->xCMD = acpWAIT;
			break;				
	
			/* Jog + */
			case acpJOG_POS:
				
				action_status = ncaction((UDINT)axis, ncPOS_MOVE, ncSTART + ncINIT);
					if ( action_status == ncOK )
						a->xCMD = acpWAIT;
			break;

			/* Jog + */
			case acpVAX_JOG_POS:	
				if(vaxis){
					action_status = ncaction((UDINT)vaxis, ncPOS_MOVE, ncSTART + ncINIT);
					if ( action_status == ncOK )
						a->xCMD = acpWAIT;
				}
				else
					a->xCMD = acpWAIT;
		break;
	
			/* Jog - */
			case acpJOG_NEG:					 				
				action_status = ncaction((UDINT)axis, ncNEG_MOVE, ncSTART + ncINIT);
					if ( action_status == ncOK )
						a->xCMD = acpWAIT;
			break;
	
			
	
			/* Start Virtaul Axis Automat */
			case acpVAX_AUT_START:
				a->Param.ParID_SetVerify	= 0;
				a->Param.ParID			= ACP10PAR_CMD_AUT_START_VAX1;
				a->Param.ParID_Data		= ncSTART;
				a->xCMD 			= acpSET_PARID;
			break;
	

	
			
			/* Stop Virtual Axix Automat */
			case acpVAX_AUT_STOP:
				a->Param.ParID_SetVerify	= 0;
				a->Param.ParID			= ACP10PAR_CMD_AUT_START_VAX1;
				a->Param.ParID_Data		= ncSTOP;
				a->xCMD 			= acpSET_PARID;
			break;
			
			/* Restart Automat */
			case acpAUT_RESTART:
				a->Param.ParID_SetVerify	= 0;
				a->Param.ParID			= ACP10PAR_CMD_AUT_START;
				a->Param.ParID_Data		= ncRESTART;
				a->xCMD 			= acpSET_PARID;
			break;
	
			/* VAXIS Restart Automat */
			case acpVAX_AUT_RESTART:
				a->Param.ParID_SetVerify	= 0;
				a->Param.ParID			= ACP10PAR_CMD_AUT_START_VAX1;
				a->Param.ParID_Data		= ncRESTART;
				a->xCMD 		= acpSET_PARID;
			break;		

			/* Absolute Move */
			case acpVAX_ABS_MOVE:	

				if(vaxis){
					action_status = ncaction((UDINT)vaxis, ncABS_MOVE, ncSTART + ncINIT);
					if ( action_status == ncOK )
						a->xCMD = acpWAIT;
				}else
					a->xCMD = acpWAIT;
						
	
			break;
	
			/* Absolute Move */
			case acpABS_MOVE:				
				action_status = ncaction((UDINT)axis, ncABS_MOVE, ncSTART + ncINIT);
					if ( action_status == ncOK )
						a->xCMD = acpWAIT;
				 			
	
			break;
	
		
	
			/* Enable Power Link User Read/Write */
			case acpPLKU_ENABLE:			
				a->msc.plnkStep = 1;			
				a->xCMD 		= acpWAIT;
			break;
	
			/* Relative Move */
			case acpREL_MOVE:
				/* Bad Parameter */
				if ( axis->move.basis.parameter.s == 0)
					a->xCMD 		= acpWAIT;
	
				else{					
					action_status = ncaction((UDINT)axis, ncREL_MOVE, ncSTART + ncINIT);
					if ( action_status == ncOK )
						a->xCMD = acpWAIT;
						
				}
			break;
	
			/* BMI2 */
			case acpBMI2:					
	
				action_status = ncaction((UDINT)axis, ncBASIS_MOVE, ncINIT);
					if ( action_status == ncOK )
						a->xCMD = acpWAIT;
				
			break;
	
			case acpMOVE_STOP:
				/*
				axis->move.stop.index.command = 0;
				if(axis->move.mode != ncOFF){
					action_status = ncaction((UDINT)axis, ncMOVE, ncSTOP);
					if ( action_status == ncOK )
						a->xCMD = acpWAIT;
				 }
				 else
					a->xCMD = acpWAIT;
				*/
				
				axis->move.stop.index.command = 1;
				
				action_status = ncaction((UDINT)axis, ncMOVE, ncSTOP);
				if ( action_status == ncOK )
					a->xCMD = acpWAIT;				 
				
			break;

			case acpMOVE_HALT:	
				
				axis->move.stop.index.command = 0;
				
				action_status = ncaction((UDINT)axis, ncMOVE, ncSTOP);
				if ( action_status == ncOK )
					a->xCMD = acpWAIT;				 
				
			break;

			case acpVAX_STOP:

				if(vaxis){
					if(vaxis->move.mode != ncOFF){
						action_status = ncaction((UDINT)vaxis, ncMOVE, ncSTOP);
						if ( action_status == ncOK )
							a->xCMD = acpWAIT;
					 }
					else
						a->xCMD = acpWAIT;
				}
				 else
					a->xCMD = acpWAIT;
			break;

			/* Stop Jogging */
			case acpJOG_STOP:
				
				axis->move.stop.index.command = 1;
				if(axis->move.mode !=ncOFF){
					action_status = ncaction((UDINT)axis, ncMOVE, ncSTOP);
					if ( action_status == ncOK )
							a->xCMD = acpWAIT;
				}
				else
					a->xCMD = acpWAIT;
					
			break;		
			
	
			case acpCMD_BRAKE_ON:
				action_status = ncaction((UDINT)axis, ncBRAKE, ncSWITCH_ON);
					if ( action_status == ncOK )
						a->xCMD = acpWAIT;
			break;
			case acpCMD_BRAKE_OFF:
				action_status = ncaction((UDINT)axis, ncBRAKE, ncSWITCH_OFF);
					if ( action_status == ncOK )
						a->xCMD = acpWAIT;
			break;
	
			case acpCONTROLLER_INIT:
				action_status = ncaction((UDINT)axis, ncCONTROLLER, ncINIT);
					if ( action_status == ncOK )
						a->xCMD = acpWAIT;
			break;
	

	
			/* Busy */
			case acpBUSY:
				a->xCMD = acpWAIT;

			break;
			
			default:
				a->xCMD = acpWAIT;
				
			break;
		}



		
		/*************************************************/
		/*************** Power Link User *****************/
		/*************************************************/

		switch (a->msc.plnkStep) {

			case 0:
				
				
			break;			

			case 1:


				if(a->msc._cycusdatto)
					a->msc.plnkStep++;
				else{
					ncalloc_Status = ncalloc(ncACP10MAN + ncPOWERLINK_IF, axis->nc_obj_inf.node_nr, ncCYCL_USER_TODRV, (16 + 2) + (a->msc.channel) * 4096, &_CYCUSDATTO);
					if (ncalloc_Status == 0) {
						a->msc._cycusdatto = (UDINT)_CYCUSDATTO;
						a->msc.plnkStep++;
					}
				}
			
			break;

			case 2:

				
				if(a->msc._cycusdatfr){
					a->msc.plnkStep++;
				}
				else{
					ncalloc_Status = ncalloc(ncACP10MAN + ncPOWERLINK_IF, axis->nc_obj_inf.node_nr, ncCYCL_USER_FRDRV, (16 + 2) +  (a->msc.channel) * 4096, &_CYCUSDATFR);
					if (ncalloc_Status == 0) {
						a->msc._cycusdatfr = (UDINT)_CYCUSDATFR;
						a->msc.plnkStep++;
					}
				}
				

			break;

			case 3:

				if(a->msc._cycusdatto && a->msc._cycusdatfr ){

					_CYCUSDATFR = (ncCYCUSDAT_typ*)a->msc._cycusdatfr;
					_CYCUSDATTO = (ncCYCUSDAT_typ*)a->msc._cycusdatto;
					
						
					/* Unpack Read Data Here: */
					memcpy( &a->Param.PlnkDataFrom[0],&_CYCUSDATFR->data[0], 16);					
					_CYCUSDATFR->cnt++;
				
					/* Pack write data */
					memcpy(&_CYCUSDATTO->data[0],&a->Param.PlnkDataTo[0], 16);
					_CYCUSDATTO->cnt++;
				}
				else{
					a->msc.plnkStep = 0;
					//a->msc._cycusdatto = 0;
					//a->msc._cycusdatfr = 0;
				}
				
			break;

			default:
				a->msc.plnkStep = 0;
			break;


			}

		/****************************************************************/
		// Error hadling by seperate state machine

		//switch(a->msc.errorStep){
		switch(a->msc.errorStep){	
			case 0:

				/* Check for an axis error or warning */	

				/* axis->monitor.status.error == ncTRUE */
				if (axis->monitor.status.error == ncTRUE) {
					a->Status.ErrorXcmd  = a->xCMD ;
					a->xCMD = acpWAIT;
					a->Command.ErrorAckAll = 0; /* Clear pending request */	
					a->Command.ErrorAck = 0; /* Clear pending request */
					a->msc.errorStep = 4;
					
					
				} 	

				/*
				if (axis->monitor.status.warning == ncTRUE) {
					if ( a->InitOptions[2] ) {						
						a->msc.errorStep	= 1;
					}
					else {
						a->msc.errorStep	= 4;
					}
					a->AcknowledgeCount = 0;
				}
				*/

			break;	
			
			case 1:
				/* Stop active movement if required */
				if ( axis->move.mode != ncOFF) {	
						axis->move.stop.index.command = 0;	
					if ((action_status = ncaction((UDINT)axis, ncMOVE, ncSTOP)) == ncOK) 
						a->msc.errorStep  = 2;	
				} 
				else 
					a->msc.errorStep  = 2;	 
					
			break;
				
			/* Switch controller off  */
			case 2:
				if ((axis->controller.status == ncON) && (axis->move.mode == ncOFF) ) {
					if ((action_status = ncaction((UDINT)axis, ncCONTROLLER, ncSWITCH_OFF)) == ncOK) 
						a->msc.errorStep  = 3;
				} 
				else if ( axis->controller.status == ncOFF) 
					a->msc.errorStep  = 4;	 
				
			break;
			
			/* Wait for controller off */
			case 3:
				if (axis->controller.status == ncOFF) 
					a->msc.errorStep  = 4;
				
			break;

			/* Wait until the number is entered in the current message record */
			case 4:
				if (axis->message.record.number != 0) {
					/* "monitor.status.error/warning" are bits in the cyclic status information */
					/* the message record will be read from the drive */
						a->msc.errorStep  = 5;
				}
								
				break;

			/* Determine text for current message record */
			case 5:
				if (ncaction((UDINT)axis, ncMESSAGE, ncTEXT) == ncOK) 
					a->msc.errorStep  = 6;
			break;
			
			/* Wait for end of text determination */
			case 6:
				//if (axis->message.text.status.lines != 0 || axis->message.text.status.error != 0  ) { 					
				if (axis->message.text.status.lines != 0  ) { 	
					a->Command.ErrorAck = 0; /* Clear pending request */								

					a->msc.errorStep  = 7;
				}
			break;
			
			/* Acknowledge current message record */
			case 7:
				if (a->Command.ErrorAck || a->Command.ErrorAckAll ) {
					if ((action_status = ncaction((UDINT)axis, ncMESSAGE, ncACKNOWLEDGE)) == ncOK ) {		
						a->Command.ErrorAck = 0;
						
						if (axis->message.record.number == 0) {
							/* all message records acknowledged */	
							a->Command.ErrorAckAll = 0;
							a->Command.ErrorAck = 0;
							a->msc.errorStep	= 8;
							a->Status.ErrorXcmd = 0;

							strcpy( a->Status.ErrorText.line1, "No Alarms" );
							strcpy( a->Status.ErrorText.line2, "Servo Okay");
							a->Status.ErrorText.line3[0] = 0;
							a->Status.ErrorText.line4[0] = 0;
							
							if(0){
								if(axis->controller.ready){
									//a->xCMD = acpCONTROLLER_ON;
									a->msc.errorStep  = 8;
								}
							}
							else {							
								a->msc.errorStep	= 0;
							}
							
						} 
						else 
							a->msc.errorStep  = 5;
					}
				}
				
			break;

			case 8:
				if(axis->controller.status == ncON){					
					a->msc.errorStep  = 0;
					
				}
			break;
			
		}


		// Out put assigmnet
		a->Status.Network_Init 		= (axis->network.init == ncTRUE );
		a->Status.ActPosition 		= axis->monitor.s ;
		a->Status.ActVelocity 		= axis->monitor.v ;		
		a->Status.Error       		= (axis->monitor.status.error == ncTRUE);
		a->Status.ErrorID     		= axis->message.record.number;
		a->Status.ControllerReady 	= (axis->controller.ready == ncTRUE);
		a->Status.Global_Init   	= (axis->global.init == ncTRUE);
		a->Status.Homed				= (axis->move.homing.status.ok == ncTRUE);
		a->Status.LagWarning		= (axis->monitor.status.warning == ncTRUE);
		a->Status.MoveMode			= axis->move.mode;
		a->Status.EnableSignal		= (axis->dig_in.status.enable == ncCLOSED);
		a->Status.ServoON			= (axis->controller.status == ncON );


		if(vaxis){
			a->Status.VaxActPos			=  vaxis->monitor.s;
			a->Status.VaxActVelocity		=  vaxis->monitor.v;
			a->Status.VaxMoveMode		= vaxis->move.mode;
		}	


		//TExec(&a->msc.tmr1);
		TON(&a->msc.tmr1);
		return 1;
	}
	
		
	
