(********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Library: ACPx
 * File: ACPx.typ
 * Author: KK
 * Created: August 30, 2011
 ********************************************************************
 * Data types of library ACPx
 ********************************************************************)

TYPE
	ACP_VAX_AUT_STATE_typ : 	STRUCT 
		select : UINT;
		ma_factor : DINT;
		comp_ma_s : DINT;
		sl_factor : DINT;
		comp_sl_s : DINT;
		cam_index : UINT;
	END_STRUCT;
	ACP_VAX_AUT_typ : 	STRUCT 
		cnt : UINT;
		state : ARRAY[0..3]OF ACP_VAX_AUT_STATE_typ;
	END_STRUCT;
	ACP_AUT_STATE_typ : 	STRUCT 
		select : UINT;
		ma_factor : DINT;
		comp_ma_s : DINT;
		sl_factor : DINT;
		comp_sl_s : DINT;
		cam_index : UINT;
	END_STRUCT;
	ACP_AUT_typ : 	STRUCT 
		cnt : UINT;
		state : ARRAY[0..3]OF ACP_AUT_STATE_typ;
	END_STRUCT;
	ACP_Cfg_typ : 	STRUCT 
		units : UDINT; (* Units at the load *)
		rev_motor : UDINT; (* Motor revolutions *)
		unit_precision : REAL;
		count_dir : USINT; (* Motor direction *)
		v_max : REAL; (*Max Speed *)
		a_max : REAL; (*Max acc/dec *)
		pos_sw_end : DINT; (* Positive SW end *)
		neg_sw_end : DINT; (* Negative SW end *)
		ParTableName : ARRAY[0..4]OF STRING[10];
		CamName : ARRAY[0..10]OF STRING[10];
		home_mode : USINT;
		lagErrorMax : REAL;
	END_STRUCT;
	ACP_CurCfg_typ : 	STRUCT 
		homePos : DINT; (* Home Position *)
		wheel_gain : REAL;
		start_offset : DINT;
		mpgen_offset : DINT;
	END_STRUCT;
	ACP_typ : 	STRUCT 
		xCMD : UINT; (*  x Command enumerator *)
		Command : acp_command_typ; (*command structure*)
		Status : acp_status_typ; (*status structure*)
		Param : acp_parameter_typ;
		msc : acp_misc_typ; (* for internal use *)
	END_STRUCT;
	acp_misc_typ : 	STRUCT  (*internal variables structure*)
		pAxis : UDINT; (* pointer to ACP10_obj *)
		pVaxis : UDINT; (* pointer to virtual ACP10_obj *)
		channel : UINT;
		pCfg : REFERENCE TO ACP_Cfg_typ;
		pCurCfg : REFERENCE TO ACP_CurCfg_typ;
		errorStep : USINT; (* error state machine step *)
		plnkStep : USINT;
		step : USINT; (*state machine step *)
		_parid_real : REAL;
		_parid_data : DINT;
		_cycusdatfr : UDINT;
		_cycusdatto : REFERENCE TO UDINT;
		d_block_seq : ACP10DATBL_typ;
		table : ACP10DATBL_typ;
		par_rec : ARRAY[0..19]OF ACP10PRADR_typ;
		i : USINT;
		idx : ARRAY[0..3]OF USINT;
		tmr1 : TON;
	END_STRUCT;
	acp_parameter_typ : 	STRUCT  (*parameter structure*)
		Position : DINT; (*target-position for MoveAbsolute-Command*)
		Distance : DINT; (*distance for MoveAdditive-Command*)
		Velocity : REAL; (*velocity for MoveVelocity-Command*)
		Acceleration : REAL; (*acceleration for commanded movements*)
		Deceleration : REAL; (*deceleration for commanded movements*)
		HomePosition : DINT; (*target-position for referencing the axis*)
		HomeMode : USINT; (*homing mode*)
		JogVelocity : REAL; (*velocity for jogging movement*)
		JogAccDec : REAL;
		Cyc_Pos : DINT;
		Cyc_Vel : REAL;
		Torq : USINT;
		ParID_Real : REAL;
		CamID : UINT;
		ParID : UINT;
		ParID_Data : DINT;
		ParID_SetVerify : BOOL;
		PlnkDataTo : ARRAY[0..15]OF USINT;
		PlnkDataFrom : ARRAY[0..15]OF USINT;
		pParTableName : REFERENCE TO USINT;
		Aut : ACP_AUT_typ;
		vaxAut : ACP_VAX_AUT_typ;
		MPGEN_SET : DINT;
		COMP_SL_S : DINT;
		SL_FACTOR : DINT;
	END_STRUCT;
	acp_error_msg_typ : 	STRUCT 
		line1 : STRING[80];
		line2 : STRING[80];
		line3 : STRING[80];
		line4 : STRING[80];
	END_STRUCT;
	acp_status_typ : 	STRUCT  (*status structure*)
		AxisName : STRING[16];
		Error : BOOL;
		ErrorID : UINT; (*ErrorID of any occured error*)
		ErrorText : acp_error_msg_typ; (*Error Text*)
		ErrorXcmd : USINT;
		ActPosition : DINT; (*actual position of the axis*)
		ActVelocity : REAL; (*actual velocity of the axis*) (*DriveStatus : MC_DRIVESTATUS_TYP; actual status of the axis*)
		fActPosition : LREAL; (*actual position of the axis*)
		fActVelocity : REAL; (*actual velocity of the axis*) (*DriveStatus : MC_DRIVESTATUS_TYP; actual status of the axis*)
		VaxActPos : DINT;
		VaxActVelocity : REAL; (*actual velocity of the axis*) (*DriveStatus : MC_DRIVESTATUS_TYP; actual status of the axis*)
		Network_Init : BOOL;
		Global_Init : BOOL;
		ServoON : BOOL;
		ControllerReady : BOOL;
		EnableSignal : BOOL;
		MoveMode : USINT;
		VaxMoveMode : USINT;
		AutState : USINT;
		Homed : BOOL;
		InPosition : BOOL;
		LagWarning : USINT;
		MPGEN_val : DINT;
		MASTER_val : DINT;
		LATCH_val : DINT;
		STATE_count : UDINT;
		ACT_STATE_Indx : USINT;
	END_STRUCT;
	acp_axisState_typ : 	STRUCT  (*axis state structure*)
		Disabled : BOOL; (*if set, axis is in state Disabled*)
		StandStill : BOOL; (*if set, axis is in state StandsStill*)
		Homing : BOOL; (*if set, axis is in state Homing*)
		Stopping : BOOL; (*if set, axis is in state Stopping*)
		DiscreteMotion : BOOL; (*if set, axis is in state DiscreteMotion*)
		ContinuousMotion : BOOL; (*if set, axis is in state ContinousMotion*)
		SynchronizedMotion : BOOL; (*if set, axis is in state SynchronizedMotion*)
		ErrorStop : BOOL; (*if set, axis is in state ErrorStop*)
	END_STRUCT;
	acp_command_typ : 	STRUCT  (*command structure*)
		Power : BOOL; (*switch on the controller*)
		Home : BOOL; (*reference the axis*)
		MoveAbsolute : BOOL; (*move to an defined position*)
		MoveAdditive : BOOL; (*move a defiened distance*)
		MoveVelocity : BOOL; (*start a movement with a defiened velocity*)
		AutomatStart : BOOL;
		AutomatStop : BOOL;
		Cyc_Abs_Start : BOOL;
		Cyc_Vel_Start : BOOL;
		Halt : BOOL; (*stop every active movement*)
		VaxMoveVelocity : BOOL;
		VaxStop : BOOL;
		VaxHome : BOOL;
		VaxMoveAdditive : BOOL;
		Stop : BOOL; (*stop every active movement as long as is set*)
		MoveJogPos : BOOL; (*move in positive direction as long as is set*)
		MoveJogNeg : BOOL; (*move in negative direction as long as is set*)
		ErrorAckAll : BOOL;
		ErrorAck : BOOL; (*reset active errors*)
	END_STRUCT;
END_TYPE
