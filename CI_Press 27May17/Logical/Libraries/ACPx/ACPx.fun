(********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Library: ACPx
 * File: ACPx.fun
 * Author: KK
 * Created: August 30, 2011
 ********************************************************************
 * Functions and function blocks of library ACPx
 ********************************************************************)

FUNCTION ACPx_Initial : DINT
	VAR_INPUT
		a : ACP_typ;
		pAxis : ACP10AXIS_typ;
		pVaxis : ACP10VAXIS_typ;
		ErrorTableName : REFERENCE TO USINT;
		channel : UINT;
		pCfg : ACP_Cfg_typ;
		pCurCfg : ACP_CurCfg_typ;
	END_VAR
END_FUNCTION

FUNCTION ACPx_Init : DINT
	VAR_INPUT
		a : ACP_typ;
		AxisName : REFERENCE TO USINT;
		VirtualAxisName : REFERENCE TO USINT;
		ErrorTableName : REFERENCE TO USINT;
		channel : UINT;
		pCfg : ACP_Cfg_typ;
		pCurCfg : ACP_CurCfg_typ;
	END_VAR
END_FUNCTION

FUNCTION ACPx_Serv : DINT
	VAR_INPUT
		a : ACP_typ;
	END_VAR
END_FUNCTION
