/*********************************************************
* FILE	 		:	
*
* OBJECTS		:
*
* OVERVIEW		:
*
* HISTORY 		:
*
* DEPENDANCIES	:
*********************************************************/

#ifndef _GLOBALDECL_
#define _GLOBALDECL_

#define SOFTWARE_VERSION    "03Jun2017"

#define FULL_ROT 65536 

#define SERVO_NUM   13

#define IMPR_CYLIND 		0
#define INSIDE_CYLIND 	1

#define ANILOX_1		2
#define ANILOX_2		3
#define ANILOX_3		4
#define ANILOX_4		5
#define ANILOX_5		6

#define COLOR_1			7
#define COLOR_2 			8
#define COLOR_3 			9
#define COLOR_4 			10
#define COLOR_5 			11

#define M_SHAFT 		12

_GLOBAL  MACH_obj  		Mach;
_GLOBAL CurCfg_obj		CurCfg  _VAR_RETAIN;

_GLOBAL ILUM_BTN_obj		Ilum_Buttons[2];

_GLOBAL plcstring 			PLC_Version[32];
_GLOBAL plcbit CfgReady;
 _GLOBAL ACP_typ			gAxis[SERVO_NUM];
_GLOBAL ACP_Cfg_typ 		ServoCfg[SERVO_NUM];
_GLOBAL ACP_CurCfg_typ 	ServoCurCfg[SERVO_NUM]  _VAR_RETAIN ;
_GLOBAL DINT 			init_Status[SERVO_NUM];

_GLOBAL	SM_obj		Screen_sm;
_GLOBAL	SCREEN_obj	Screen;
_GLOBAL HS_obj		LoadCfg, RestoreDefault, StoreDefault;
_GLOBAL SM_obj		Master_sm;

_GLOBAL SM_obj		 	AXIS_sm[SERVO_NUM];
_GLOBAL AXIS_Color_typ  	mAXIS[SERVO_NUM];

_GLOBAL float KK_temp_Length;

#endif

