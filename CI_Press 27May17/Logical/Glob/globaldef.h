/*********************************************************
* FILE	 		:	
*
* OBJECTS		:
*
* OVERVIEW		:
*
* HISTORY 		:
*
* DEPENDANCIES	:
*********************************************************/
#define DONE 0

#define COLOR_NUM			5
#define IMPR_NUM			2

#define MS_STOP 	1
#define MS_RUN 	2
#define MS_STEP	3

#define OFF 		0
#define ON		1
#define AUTO		2


#define CMD_FWD_FINE 		1
#define CMD_REV_FINE 		2
#define CMD_FWD_COARSE 	3
#define CMD_REV_COARSE 	4

#define CMD_OFF 			5
#define CMD_ON 				6
#define CMD_AUTO 			7

#define CMD_JOG_POSITIVE 	8
#define CMD_ACCEPT_POS 	9


#define STAT_OFF      0
#define STAT_W_AUT	1
#define STAT_ALIGN	2
#define STAT_SYNCH	3
#define STAT_PAUSE	4
#define STAT_ERROR	5
#define STAT_JOG		6
#define STAT_STOP_JOG		7
#define STAT_RESYN	8

#define BTN_OFF		0
#define BTN_RED  	1
#define BTN_YELLOW 	2
#define BTN_GREEN	3
#define BTN_GRAY	4	

typedef struct CurCfg_obj
{	
	USINT PrintCylind_Mode[COLOR_NUM] ;
	USINT AniloxRoller_Mode[COLOR_NUM];	
	USINT DrBlade_Mode[COLOR_NUM];
	USINT InkPump_Mode[COLOR_NUM];
	USINT Color_Active[COLOR_NUM];
	USINT Color5Dir;
	USINT Mode;
	float   Anilox_Dim[COLOR_NUM];
	float   GlobalAnilox_Circum;	
	float   BlankLen;
	float	   MainVaxSpeed;
	float	   MainVaxAcc;
	float	   CoarseDist;
	float   FineDist;
	float   IncrVel;
	float   AlignVel;
	float   IncrAcc;
	float   AlignAcc;
	float   AniloxVel;
	float   AniloxAcc;
	float   AngageVel; //RPM
	float   AngageDly;
	float   AniloxDly;
	float   JogAcc;
	float   JogVel;

}CurCfg_obj;

typedef struct MACH_Cmd_obj
{	
	USINT MainShaft;
	USINT PrintCylind[COLOR_NUM];
	USINT AniloxRoller[COLOR_NUM];
	USINT InkPump[COLOR_NUM];
	plcbit Engage;
	plcbit AniloxRun;
	//USINT AniloxRoller[COLOR_NUM];
	//USINT ImpressionCylind[IMPR_NUM];

}MACH_Cmd_obj;



typedef struct MACH_obj
{	
	CurCfg_obj* pCurCfg;
	MACH_Cmd_obj Cmd;
	MACH_Cmd_obj Status;
	UINT ESTOP;
	UINT Alarm;
	USINT shft_step;
	float RPM;
	TON_typ Tmr;

}MACH_obj;

typedef struct ILUM_BTN_obj
{	
	plcbit Input[6];
	plcbit Out_Bits[24];	
	UINT Out_Color[6];
	plcbit _Input[6];

}ILUM_BTN_obj;

typedef struct SCREEN_obj
{	

	//plcbit btnMasterOn[5];
	//plcbit btnMasterOff[5];
	//plcbit btnMasterAuto[5];
	unsigned short SetPage;
	unsigned short ReadPage;
	unsigned short GotoScreen;
	unsigned short ReturnScreen;
	unsigned short State;
	unsigned short StateGlobal;
	unsigned short Index;
	unsigned short NavMenu;
	unsigned short GlbBtn;
	plcbit Alarms[32];
	plcbit SetInVisible;
	plcbit SetVisible;
	plcbit btn[20];
	plcbit btnBar[30];
	plcbit btnMenu[10];
	plcbit btnSave;
	plcbit btnIndex;
	unsigned short vis[50];
	unsigned short visBar[30];
	unsigned short visSave;
	unsigned short colorBackground;
	unsigned short color[55];
	unsigned short colorBar[10];
	unsigned short colorSave;
	signed long Param[20];
	float fParam[20];
	unsigned short Language;
	unsigned short Units;
	unsigned long LifeSign;
	unsigned char AccessLevel;
	unsigned short ModeColor[10];
	unsigned short Passcode;
	unsigned short MustRestart;
	plcstring AlarmMsg[64];
	
} SCREEN_obj;


/////////////////////////////////////////////

typedef struct AXIS_Color_MSC_typ
{	
	struct TON tmr1;
	struct TON tmr2;
	struct TON watchDog;
	signed long _COMP_SL_S;
	USINT _Active;
	DINT startOffset;
		
}AXIS_Color_MSC_typ;

typedef struct AXIS_Color_Param_typ
{
	signed long COMP_SL_S;
	signed long COMP_MA_S;

	
} AXIS_Color_Param_typ;

typedef struct AXIS_Color_CMD_typ
{	
	USINT Synchro;
	USINT Active;
	USINT JogFWD;
	USINT Resynchro;
	USINT Aligment;
	
	
} AXIS_Color_CMD_typ;

typedef struct AXIS_Color_Status_typ
{	
	//unsigned char Synchronized;
	unsigned char Status;
	float MasterAngle;
	float Angle;
	float RelativeAngle;
	float VaxAngle;
	float RelativeDist;
	float OffsetDist;
	float OffsetError;
		
} AXIS_Color_Status_typ;

typedef struct AXIS_Color_typ
{	
	struct AXIS_Color_CMD_typ Cmd;
	struct AXIS_Color_Param_typ Param;
	struct AXIS_Color_Status_typ Status;
	struct AXIS_Color_MSC_typ msc;
	
} AXIS_Color_typ;


/*===COLORS===*/

#define GREEN				10
#define DARK_GREEN			2
#define BLUE					1
#define BABY_BLUE			11
#define TEAL					168
#define RED					0x33
#define YELLOW				46
#define ORANGE				48
#define BLACK				0
#define WHITE				15
#define GRAY					7
#define DARK_NAVY			219
#define DARK_GRAY			8
#define LIGHT_GRAY			227
#define CYAN					196
#define MAGENTA				21
#define BROWN				86
#define LIGHT_BLUE			52
#define CHECK				1
#define UNCHECK				0
#define VISIBLE				0
#define INVISIBLE			1
#define YELLOW_ON_DBLUE		0x2EDB
#define DBLUE_ON_YELLOW		0xDB2E
#define WHITE_ON_RED		0x0F33
#define RED_ON_WHITE		0x330F
#define WHITE_ON_BLACK		0x0F00
#define BLACK_ON_WHITE		0x000F
#define BLACK_ON_LYELLOW	0x001C
#define BLACK_ON_MAGENTA	0x0015
#define YELLOW_ON_BLUE		0x2E01
#define BLUE_ON_YELLOW		0x012E