/*********************************************************
* FILE	 		:	
*
* OBJECTS		:
*
* OVERVIEW		:
*
* HISTORY 		:
*
* DEPENDANCIES	:
*********************************************************/
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <..\Glob\globaldef.h>
#include <..\Glob\globaldecl.h>

#define MGEN_OFF 0
#define MPGEN_TIME 1
#define MPGEN_POS 2

DINT _COMP_SL_S[SERVO_NUM];
DINT KK_Start_Pos[SERVO_NUM];

_LOCAL ACP10AXIS_typ *axis;
_LOCAL ACP10VAXIS_typ *vaxis;



//void AXIS_Rot_Style(UINT startIndx , UINT endIndx ,  SM_obj* sm ,  AXIS_GRP_typ* mgr);
void AXIS_Color(ACP_typ *g ,  ACP_CurCfg_typ *curCfg ,   AXIS_Color_typ *m ,   SM_obj *sm , MACH_obj *mach );
void AXIS_Anilox(ACP_typ *g ,  ACP_CurCfg_typ *curCfg ,   AXIS_Color_typ *m ,   SM_obj *sm , MACH_obj *mach );

DINT AbsFromDINT(DINT x);
void _INIT ProgramInit(void)
{
	
}

void _CYCLIC ProgramCyclic(void)
{
int i;

	Mach.pCurCfg = &CurCfg;

	if(!CfgReady)
		return;

	for ( i = IMPR_CYLIND; i <= INSIDE_CYLIND; i++){

		mAXIS[i].Param.COMP_SL_S = (DINT) (Mach.pCurCfg->BlankLen * ServoCfg[i].unit_precision);

		//mAXIS[i].Param.COMP_SL_S = FULL_ROT;
		
		AXIS_Anilox(&gAxis[i],  &ServoCurCfg[i],   &mAXIS[i],   &AXIS_sm[i] , &Mach);

	}	
	
	for ( i = ANILOX_1; i <= ANILOX_5; i++){

		
		mAXIS[i].Param.COMP_SL_S = (DINT) (Mach.pCurCfg->BlankLen * ServoCfg[i].unit_precision);

		/*	
		Mach.pCurCfg->GlobalAnilox_Circum = 102.13 * M_PI;

		if( Mach.pCurCfg->GlobalAnilox_Circum >  0)
			mAXIS[i].Param.COMP_SL_S = (DINT) (Mach.pCurCfg->BlankLen * FULL_ROT / Mach.pCurCfg->GlobalAnilox_Circum);
		*/

		
		
		AXIS_Anilox(&gAxis[i],  &ServoCurCfg[i],   &mAXIS[i],   &AXIS_sm[i] , &Mach);

	}	
	
	
	for ( i = COLOR_1; i <= COLOR_5; i++)	
		AXIS_Color(&gAxis[i],  &ServoCurCfg[i],   &mAXIS[i],   &AXIS_sm[i] , &Mach);




	//////////   Main Shaft  ///////////
	
	axis = gAxis[M_SHAFT].msc.pAxis;

	if(axis){
		mAXIS[M_SHAFT].Status.MasterAngle = ((float)(axis->monitor.s%FULL_ROT))/FULL_ROT;

	}
////////////////////////////////////


///////////////////////////////////


}

void _EXIT ProgramExit(void)
{
	
}

void AXIS_Anilox(ACP_typ *g ,  ACP_CurCfg_typ *curCfg ,   AXIS_Color_typ *m ,   SM_obj *sm  , MACH_obj *mach){
DINT pos;

	if(g->msc._cycusdatfr){
		
			// From Acopos
			memcpy(&g->Status.MPGEN_val, &g->Param.PlnkDataFrom[0],  4); //	MPGEN
			memcpy(&g->Status.MASTER_val, &g->Param.PlnkDataFrom[4], 4); // Master
			memcpy(&g->Status.LATCH_val,&g->Param.PlnkDataFrom[8],4);  //LATCH_VALUE+0
			//memcpy(&g->Status.STATE_count,&g->Param.PlnkDataFrom[12],4); // STATE COUNT	
			memcpy(&g->Status.ACT_STATE_Indx ,&g->Param.PlnkDataFrom[12],1); // AUT_ACT_ST_INDEX	

			
			m->Status.MasterAngle =  ((float)(g->Status.MASTER_val % FULL_ROT)) / FULL_ROT;
			m->Status.Angle =  ((float)(g->Status.ActPosition % FULL_ROT)) / FULL_ROT;
			m->Status.VaxAngle =  ((float)(g->Status.VaxActPos % FULL_ROT)) / FULL_ROT;
			m->Status.RelativeAngle = ((float)((g->Status.ActPosition - g->Status.MASTER_val )%FULL_ROT))/ FULL_ROT;


			m->Status.RelativeDist =  m->Status.RelativeAngle * mach->pCurCfg->BlankLen;

			m->Status.OffsetDist =( (float)(curCfg->start_offset %FULL_ROT)) / FULL_ROT * mach->pCurCfg->BlankLen;

	}	

	if(!m->Cmd.Synchro
		|| g->Status.Error){
		
		if((g->Status.MoveMode == ncACTIVE)
			&& (g->xCMD == 0))
			g->xCMD = acpAUT_STOP;

		sm->Step = 0;

	}	

	switch(sm->Step){


		case 0: sm_text("W: INIT" ,sm);

			g->Command.Power = 0;	

			if(g->Status.Error)
				m->Status.Status = STAT_ERROR;
			else				
				m->Status.Status = STAT_OFF;

			if(m->Cmd.Synchro){

				m->Status.Status = STAT_W_AUT;
				sm->Step ++;

			}	
			
		break;		
		
		case 1: sm_text("W: Power Off" ,sm);

			if((g->msc.step == 60)
				&& (g->xCMD == 0)){

				g->xCMD  = acpVAX_STOP;
				sm->Step ++;

			}	
			
		break;

		case 2: sm_text("W: VAX Stop" ,sm);

			if((g->Status.VaxMoveMode == ncOFF) 
				&& (g->xCMD == 0)){
				
				g->xCMD  = acpVAX_HOME;
				TimerStart( &m->msc.tmr1, 0.5);
				sm->Step ++;
			}	

		break;

		case 3: sm_text("W: VAX Home" ,sm);

			if(!(TimerIsTimeOut(&m->msc.tmr1)))
				break;

			if((g->Status.VaxActPos == 0) 
				&& (g->xCMD == 0)){

				g->Param.ParID_Data = g->Status.MASTER_val;
				
				g->Param.ParID = ACP10PAR_AUT_MA_S_START;
				g->xCMD = acpSET_PARID;	

				TimerStart( &m->msc.tmr1, 0.5);
				sm->Step ++;
			}	


		break;	

		case 4: sm_text("W: AUT_MA_S_START " ,sm);

			if(!(TimerIsTimeOut(&m->msc.tmr1)))
				break;

			if(!g->xCMD){

				switch(m->Cmd.Active){


					case 1: // Active - start turning

						
						g->Param.Aut.state[0].select = 1;
						g->Param.Aut.state[0].cam_index = 0xFFFE;
						g->Param.Aut.state[0].comp_ma_s = m->Param.COMP_MA_S*2;							
						g->Param.Aut.state[0].comp_sl_s = m->Param.COMP_SL_S;	
						g->Param.Aut.state[0].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[0].sl_factor = m->Param.COMP_SL_S;	
						
																		
						g->Param.Aut.state[1].select = 1;
						g->Param.Aut.state[1].cam_index = 0xFFFE;
						g->Param.Aut.state[1].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[1].comp_sl_s = m->Param.COMP_SL_S;	
						g->Param.Aut.state[1].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[1].sl_factor = m->Param.COMP_SL_S;	

						g->Param.Aut.state[2].select = 1;
						g->Param.Aut.state[2].cam_index = 0xFFFE;
						g->Param.Aut.state[2].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[2].comp_sl_s = m->Param.COMP_SL_S;	
						g->Param.Aut.state[2].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[2].sl_factor = m->Param.COMP_SL_S;	
										
					break;

					default: // disable - stop turning

						
						g->Param.Aut.state[0].select = 1;
						g->Param.Aut.state[0].cam_index = 0xFFFE;
						g->Param.Aut.state[0].comp_ma_s = m->Param.COMP_MA_S*2;							
						g->Param.Aut.state[0].comp_sl_s = 0;
						g->Param.Aut.state[0].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[0].sl_factor = 0;						


						g->Param.Aut.state[1].select = 1;
						g->Param.Aut.state[1].cam_index = 0xFFFE;
						g->Param.Aut.state[1].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[1].comp_sl_s = 0;
						g->Param.Aut.state[1].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[1].sl_factor = 0;	

						g->Param.Aut.state[2].select = 1;
						g->Param.Aut.state[2].cam_index = 0xFFFE;
						g->Param.Aut.state[2].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[2].comp_sl_s = 0;
						g->Param.Aut.state[2].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[2].sl_factor = 0;	
						

					break;	

				}	

				m->msc._Active = m->Cmd.Active;
				m->msc._COMP_SL_S =  m->Param.COMP_SL_S;

				TimerStart( &m->msc.tmr1, 0.5);

				g->xCMD = acpAUT_WRITE;

				sm->Step ++;

			}


		break;

		case 5: sm_text("W: All Param" ,sm);

			if(!(TimerIsTimeOut(&m->msc.tmr1)))
				break;

			if(!g->xCMD){
				
				g->Command.Power = 1;
				sm->Step ++;
				
			}
			
			
		break;

		case 6: sm_text("W: Power ON " ,sm);

			if(!(TimerIsTimeOut(&m->msc.tmr1)))
				break;

			if((!g->xCMD) 
				&& ( g->Status.ServoON)){

				g->Command.AutomatStart = 1;

				sm->Step ++;

			}

		break;	

		case 7: sm_text("W: Aut Start " ,sm);

			
			if((!g->xCMD)
				&& (g->Status.MoveMode == ncACTIVE)
				&& (g->Status.AutState != 0xFFFF)){

				m->Status.Status = STAT_ALIGN;

				sm->Step ++;
			}	


		break;	

		case 8: sm_text("W: State 1" ,sm);

			if(g->Status.ACT_STATE_Indx == 1)
				sm->Step ++;


		break;


		case 9: sm_text("W: State 2" ,sm);

			if(g->Status.ACT_STATE_Indx == 2)
				sm->Step = 20;


		break;




		case 20:  sm_text("SYNCHRO" ,sm);

			if(g->xCMD)
				break;

			if(m->msc._Active)
				m->Status.Status = STAT_SYNCH;
			else
				m->Status.Status = STAT_PAUSE;

			if( m->msc._Active != m->Cmd.Active){

				switch(m->Cmd.Active){


					case 1: // Active - start turning

						g->Param.Aut.state[0].select = 1;
						g->Param.Aut.state[0].cam_index = 0xFFFE;
						g->Param.Aut.state[0].comp_ma_s = m->Param.COMP_MA_S * 4;							
						g->Param.Aut.state[0].comp_sl_s = m->Param.COMP_SL_S *2;	
						g->Param.Aut.state[0].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[0].sl_factor = m->Param.COMP_SL_S;		

						g->Param.Aut.state[1].select = 0;
						g->Param.Aut.state[1].cam_index = 0xFFFE;
						g->Param.Aut.state[1].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[1].comp_sl_s = m->Param.COMP_SL_S;	
						g->Param.Aut.state[1].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[1].sl_factor = m->Param.COMP_SL_S;	

						
						g->Param.Aut.state[2].select = 0;
						g->Param.Aut.state[2].cam_index = 0xFFFE;
						g->Param.Aut.state[2].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[2].comp_sl_s = m->Param.COMP_SL_S;	
						g->Param.Aut.state[2].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[2].sl_factor = m->Param.COMP_SL_S;	
						
						
					
					break;

					default: // disable - stop turning

						g->Param.Aut.state[0].select = 1;
						g->Param.Aut.state[0].cam_index = 0xFFFE;
						g->Param.Aut.state[0].comp_ma_s = m->Param.COMP_MA_S * 4;							
						g->Param.Aut.state[0].comp_sl_s = m->Param.COMP_SL_S * 2;
						g->Param.Aut.state[0].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[0].sl_factor = 0;		

						g->Param.Aut.state[1].select = 0;
						g->Param.Aut.state[1].cam_index = 0xFFFE;
						g->Param.Aut.state[1].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[1].comp_sl_s = 0;
						g->Param.Aut.state[1].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[1].sl_factor = 0;	

						
						g->Param.Aut.state[2].select = 0;
						g->Param.Aut.state[2].cam_index = 0xFFFE;
						g->Param.Aut.state[2].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[2].comp_sl_s = 0;
						g->Param.Aut.state[2].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[2].sl_factor = 0;	
						


					break;	

				}	

				m->msc._Active = m->Cmd.Active;
				g->xCMD = acpAUT_WRITE;

				sm->Step ++;


			}			
			else if(m->msc._COMP_SL_S!=  m->Param.COMP_SL_S){

				if(g->Param.Aut.state[1].select == 0){

					g->Param.Aut.state[1].select = 1;
					g->Param.Aut.state[1].cam_index = 0xFFFE;
					g->Param.Aut.state[1].comp_ma_s = m->Param.COMP_MA_S;							
					g->Param.Aut.state[1].comp_sl_s = m->Param.COMP_SL_S;	;
					g->Param.Aut.state[1].ma_factor = m->Param.COMP_MA_S;						
					g->Param.Aut.state[1].sl_factor = m->Param.COMP_SL_S;	

					
					g->Param.Aut.state[2].select = 1;
					g->Param.Aut.state[2].cam_index = 0xFFFE;
					g->Param.Aut.state[2].comp_ma_s = m->Param.COMP_MA_S;							
					g->Param.Aut.state[2].comp_sl_s = m->Param.COMP_SL_S;	;
					g->Param.Aut.state[2].ma_factor = m->Param.COMP_MA_S;						
					g->Param.Aut.state[2].sl_factor = m->Param.COMP_SL_S;	
					
					m->msc._COMP_SL_S =  m->Param.COMP_SL_S;


					g->xCMD = acpAUT_WRITE;

				}							
					

			}

			
		break;	


		case 21:  sm_text("W: Param Sent" ,sm);

			if(!g->xCMD){

				g->xCMD = acpAUT_SIGNAL1;
				sm->Step ++;
			}	


		break;


		case 22: sm_text("W: State 1" ,sm);

			if(g->Status.ACT_STATE_Indx == 1){

				g->Param.Aut.state[1].select = 1;
				g->Param.Aut.state[2].select = 1;
				g->xCMD = acpAUT_WRITE;
				sm->Step ++;
			}	

		break;


		case 23: sm_text("W: State 2" ,sm);

			if(g->Status.ACT_STATE_Indx == 2){

				g->xCMD = acpAUT_RESETSIGNAL1;
				sm->Step  = 20;
			}	

		break;


		

				

	}

	/********************************************************/		
		
	memcpy(&g->Param.PlnkDataTo[0], &g->Param.MPGEN_SET, 4); //MPGEN
	memcpy(&g->Param.PlnkDataTo[4],  &g->Param.COMP_SL_S, 4);
	memcpy(&g->Param.PlnkDataTo[8],  &g->Param.SL_FACTOR, 4);	

	/********************************************************/
	
	sm_serv(sm);
	TimerExecute(&m->msc.tmr1);
	TimerExecute(&m->msc.tmr2);
	TimerExecute(&m->msc.watchDog);



}
////////////////////////////////////////////////////////////

void AXIS_Color(ACP_typ *g ,  ACP_CurCfg_typ *curCfg ,   AXIS_Color_typ *m ,   SM_obj *sm , MACH_obj *mach ){
ACP10VAXIS_typ *vaxis;	
DINT pos;

	if(g->msc._cycusdatfr){
		
			// From Acopos
			memcpy(&g->Status.MPGEN_val, &g->Param.PlnkDataFrom[0],  4); //	MPGEN
			memcpy(&g->Status.MASTER_val, &g->Param.PlnkDataFrom[4], 4); // Master
			memcpy(&g->Status.LATCH_val,&g->Param.PlnkDataFrom[8],4);  //LATCH_VALUE+0
			//memcpy(&g->Status.STATE_count,&g->Param.PlnkDataFrom[12],4); // STATE COUNT	
			memcpy(&g->Status.ACT_STATE_Indx ,&g->Param.PlnkDataFrom[12],1); // AUT_ACT_ST_INDEX	

			
			m->Status.MasterAngle =  ((float)(g->Status.MASTER_val % FULL_ROT)) / FULL_ROT;
			//m->Status.MasterAngle =  ((float)(gAxis[INSIDE_CYLIND].Status.VaxActPos% FULL_ROT)) / FULL_ROT;
			m->Status.Angle =  ((float)(g->Status.ActPosition % FULL_ROT)) / FULL_ROT;
			m->Status.VaxAngle =  ((float)(g->Status.VaxActPos % FULL_ROT)) / FULL_ROT;
			m->Status.RelativeAngle = ((float)((g->Status.ActPosition - g->Status.MASTER_val )%FULL_ROT))/ FULL_ROT;

			if(m->Status.RelativeAngle < 0.0)
				m->Status.RelativeAngle += 1.0;

			m->Status.RelativeDist =  m->Status.RelativeAngle * mach->pCurCfg->BlankLen;

			m->Status.OffsetDist =( (float)((curCfg->start_offset %FULL_ROT))) / FULL_ROT * mach->pCurCfg->BlankLen;

			m->Status.OffsetError = m->Status.RelativeDist - m->Status.OffsetDist;

			
			

	}	

	if(!m->Cmd.Synchro
		|| g->Status.Error){
		
		if((g->Status.MoveMode == ncACTIVE)
			&& (g->xCMD == 0))
			g->xCMD = acpAUT_STOP;

		sm->Step = 0;

	}	

	switch(sm->Step){


		case 0: sm_text("W: INIT" ,sm);

			g->Command.Power = 0;	

			if(g->Status.Error)
				m->Status.Status = STAT_ERROR;
			else				
				m->Status.Status = STAT_OFF;

			if(m->Cmd.Synchro){

				m->Status.Status = STAT_W_AUT;
				sm->Step ++;

			}	
			
		break;		
		
		case 1: sm_text("W: Power Off" ,sm);

			if((g->msc.step == 60)
				&& (g->xCMD == 0)){

				g->xCMD  = acpVAX_STOP;
				sm->Step ++;

			}	
			
		break;

		case 2: sm_text("W: VAX Stop" ,sm);

			if((g->Status.VaxMoveMode == ncOFF) 
				&& (g->xCMD == 0)){
				
				g->xCMD  = acpVAX_HOME;
				TimerStart( &m->msc.tmr1, 1.0);
				sm->Step ++;
			}	

		break;

		case 3: sm_text("W: VAX Home" ,sm);

			if(!(TimerIsTimeOut(&m->msc.tmr1)))
				break;

			if((g->Status.VaxActPos == 0) 
				&& (g->xCMD == 0)){

				pos = (g->Status.ActPosition % FULL_ROT);
			
				g->Param.ParID_Data = (g->Status.MASTER_val - (g->Status.MASTER_val%FULL_ROT)) + FULL_ROT - pos;
				
				g->Param.ParID = ACP10PAR_AUT_MA_S_START;
				g->xCMD = acpSET_PARID;	

				TimerStart( &m->msc.tmr1, 0.5);
				sm->Step ++;
			}	


		break;	

		case 4: sm_text("W: AUT_MA_S_START " ,sm);

			if(!(TimerIsTimeOut(&m->msc.tmr1)))
				break;

			if(!g->xCMD){

						
					m->Param.COMP_MA_S = FULL_ROT;
					m->Param.COMP_SL_S = FULL_ROT;
					
				switch(m->Cmd.Active){


					case 1: // Active - start turning

						
						g->Param.Aut.state[0].select = 1;
						g->Param.Aut.state[0].cam_index = 0xFFFE;
						g->Param.Aut.state[0].comp_ma_s = m->Param.COMP_MA_S*2;							
						g->Param.Aut.state[0].comp_sl_s = m->Param.COMP_SL_S;	
						g->Param.Aut.state[0].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[0].sl_factor = m->Param.COMP_SL_S;	
						
																		
						g->Param.Aut.state[1].select = 1;
						g->Param.Aut.state[1].cam_index = 0xFFFE;
						g->Param.Aut.state[1].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[1].comp_sl_s = m->Param.COMP_SL_S;	
						g->Param.Aut.state[1].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[1].sl_factor = m->Param.COMP_SL_S;	

						g->Param.Aut.state[2].select = 1;
						g->Param.Aut.state[2].cam_index = 0xFFFE;
						g->Param.Aut.state[2].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[2].comp_sl_s = m->Param.COMP_SL_S;	
						g->Param.Aut.state[2].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[2].sl_factor = m->Param.COMP_SL_S;	
										
					break;

					default: // disable - stop turning

						
						g->Param.Aut.state[0].select = 1;
						g->Param.Aut.state[0].cam_index = 0xFFFE;
						g->Param.Aut.state[0].comp_ma_s = m->Param.COMP_MA_S*2;							
						g->Param.Aut.state[0].comp_sl_s = 0;
						g->Param.Aut.state[0].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[0].sl_factor = 0;						


						g->Param.Aut.state[1].select = 1;
						g->Param.Aut.state[1].cam_index = 0xFFFE;
						g->Param.Aut.state[1].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[1].comp_sl_s = 0;
						g->Param.Aut.state[1].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[1].sl_factor = 0;	

						g->Param.Aut.state[2].select = 1;
						g->Param.Aut.state[2].cam_index = 0xFFFE;
						g->Param.Aut.state[2].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[2].comp_sl_s = 0;
						g->Param.Aut.state[2].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[2].sl_factor = 0;	
						

					break;	

				}			
						

				m->msc._Active = m->Cmd.Active;
				m->msc._COMP_SL_S =  m->Param.COMP_SL_S;

				TimerStart( &m->msc.tmr1, 0.5);

				g->xCMD = acpAUT_WRITE;

				sm->Step ++;

			}


		break;

		case 5: sm_text("W: All Param" ,sm);

			if(!(TimerIsTimeOut(&m->msc.tmr1)))
				break;

			if(!g->xCMD){
				
				g->Command.Power = 1;
				sm->Step ++;
				
			}
			
			
		break;

		case 6: sm_text("W: Power ON " ,sm);

			if(!(TimerIsTimeOut(&m->msc.tmr1)))
				break;

			if((!g->xCMD) 
				&& ( g->Status.ServoON)){

				g->Command.AutomatStart = 1;

				sm->Step ++;

			}

		break;	

		case 7: sm_text("W: Aut Start " ,sm);

			
			if((!g->xCMD)
				&& (g->Status.MoveMode == ncACTIVE)
				&& (g->Status.AutState != 0xFFFF)){	

				m->Status.Status = STAT_ALIGN;				

				sm->Step ++;
			}	


		break;	

		case 8: sm_text("W: State 1" ,sm);

			if(g->Status.ACT_STATE_Indx >= 1){

				vaxis = g->msc.pVaxis;
				
				
				if(vaxis){

					m->msc.startOffset = (g->Status.MASTER_val%FULL_ROT) -(g->Status.ActPosition%FULL_ROT) ;						
					vaxis->move.basis.parameter.s = (g->msc.pCurCfg->start_offset + m->msc.startOffset + FULL_ROT)%FULL_ROT;
					
					//vaxis->move.basis.parameter.s = (g->msc.pCurCfg->start_offset + FULL_ROT)%FULL_ROT;
					
					vaxis->move.basis.parameter.v_pos = (mach->pCurCfg->AlignVel /mach->pCurCfg->BlankLen) * FULL_ROT;
					vaxis->move.basis.parameter.a1_pos = mach->pCurCfg->AlignAcc;
					vaxis->move.basis.parameter.a2_pos = vaxis->move.basis.parameter.a1_pos;

					g->xCMD = acpVAX_ABS_MOVE;
				}

				sm->Step++;

			}


		break;


		case 9: sm_text("W: State 2" ,sm);

			if(g->Status.ACT_STATE_Indx == 2){

				vaxis = g->msc.pVaxis;			

				sm->Step++;

			}


		break;


		case 10: sm_text("W: Aligmnet" ,sm);
			
			//if( g->Status.VaxActPos == g->msc.pCurCfg->start_offset) {		
			if( g->Status.VaxMoveMode == ncOFF) {	
					m->Cmd.Aligment	= 1;
					sm->Step = 20;

			}		

		break;	


		case 20:  sm_text("SYNCHRO" ,sm);

			if(g->xCMD)
				break;

			
			if(!m->Cmd.Aligment){
				
				if(m->msc._Active)
					m->Status.Status = STAT_SYNCH;
				else
					m->Status.Status = STAT_PAUSE;
			}
			//////////////////////////////////////////////////

			m->Cmd.Resynchro = 0;

			if(m->Cmd.JogFWD)
				sm->Step = 30;
			
			///////////////////////////////////////////////////

			if(m->Cmd.Aligment && (Mach.RPM == 0.0)){

				m->Cmd.Aligment = 0;

				vaxis = g->msc.pVaxis;				
				
				if(vaxis){

					/*
					// To dobrze dzialalo 
					m->msc.startOffset = (g->Status.MASTER_val%FULL_ROT) -(g->Status.ActPosition%FULL_ROT) ;						
					vaxis->move.basis.parameter.s = (m->msc.startOffset + g->msc.pCurCfg->start_offset) ;
					*/

					/* To zle
					m->msc.startOffset = (g->Status.MASTER_val%FULL_ROT) -(g->Status.ActPosition%FULL_ROT) ;						
					vaxis->move.basis.parameter.s = ( (m->msc.startOffset + g->msc.pCurCfg->start_offset) + FULL_ROT) %FULL_ROT;
					*/

					m->msc.startOffset = (g->Status.MASTER_val%FULL_ROT) -(g->Status.ActPosition%FULL_ROT) ;
					vaxis->move.basis.parameter.s = (m->msc.startOffset + g->msc.pCurCfg->start_offset)%FULL_ROT;

					// To trzeba przemyslec
					/*
					if(AbsFromDINT(vaxis->move.basis.parameter.s) > (FULL_ROT/2)){

						if(vaxis->move.basis.parameter.s > 0)
							vaxis->move.basis.parameter.s = FULL_ROT - vaxis->move.basis.parameter.s;
						else
							vaxis->move.basis.parameter.s += FULL_ROT;
											

					}
					*/
					
					vaxis->move.basis.parameter.v_pos = (mach->pCurCfg->AlignVel /mach->pCurCfg->BlankLen) * FULL_ROT;
					vaxis->move.basis.parameter.a1_pos = mach->pCurCfg->AlignAcc;
					vaxis->move.basis.parameter.a2_pos = vaxis->move.basis.parameter.a1_pos;

					g->xCMD = acpVAX_REL_MOVE;
				}
			}


			//////////////////////////////////////////////////

			if( m->msc._Active != m->Cmd.Active){

				switch(m->Cmd.Active){


					case 1: // Active - start turning

						g->Param.Aut.state[0].select = 1;
						g->Param.Aut.state[0].cam_index = 0xFFFE;
						g->Param.Aut.state[0].comp_ma_s = m->Param.COMP_MA_S * 4;							
						g->Param.Aut.state[0].comp_sl_s = m->Param.COMP_SL_S * 2;	
						g->Param.Aut.state[0].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[0].sl_factor = m->Param.COMP_SL_S;		

						g->Param.Aut.state[1].select = 0;
						g->Param.Aut.state[1].cam_index = 0xFFFE;
						g->Param.Aut.state[1].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[1].comp_sl_s = m->Param.COMP_SL_S;	
						g->Param.Aut.state[1].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[1].sl_factor = m->Param.COMP_SL_S;	

						
						g->Param.Aut.state[2].select = 0;
						g->Param.Aut.state[2].cam_index = 0xFFFE;
						g->Param.Aut.state[2].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[2].comp_sl_s = m->Param.COMP_SL_S;	
						g->Param.Aut.state[2].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[2].sl_factor = m->Param.COMP_SL_S;	
						
						
					
					break;

					default: // disable - stop turning

						g->Param.Aut.state[0].select = 1;
						g->Param.Aut.state[0].cam_index = 0xFFFE;
						g->Param.Aut.state[0].comp_ma_s = m->Param.COMP_MA_S * 4;							
						g->Param.Aut.state[0].comp_sl_s = m->Param.COMP_SL_S * 2;
						g->Param.Aut.state[0].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[0].sl_factor = 0;		

						g->Param.Aut.state[1].select = 0;
						g->Param.Aut.state[1].cam_index = 0xFFFE;
						g->Param.Aut.state[1].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[1].comp_sl_s = 0;
						g->Param.Aut.state[1].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[1].sl_factor = 0;	

						
						g->Param.Aut.state[2].select = 0;
						g->Param.Aut.state[2].cam_index = 0xFFFE;
						g->Param.Aut.state[2].comp_ma_s = m->Param.COMP_MA_S;							
						g->Param.Aut.state[2].comp_sl_s = 0;
						g->Param.Aut.state[2].ma_factor = m->Param.COMP_MA_S;						
						g->Param.Aut.state[2].sl_factor = 0;	
						


					break;	

				}	

				m->msc._Active = m->Cmd.Active;
				g->xCMD = acpAUT_WRITE;

				sm->Step ++;


			}	
			
		break;	


		case 21:  sm_text("W: Param Sent" ,sm);

			if(!g->xCMD){

				g->xCMD = acpAUT_SIGNAL1;
				sm->Step ++;
			}	


		break;


		case 22: sm_text("W: State 1" ,sm);

			if(g->Status.ACT_STATE_Indx == 1){

				g->Param.Aut.state[1].select = 1;
				g->Param.Aut.state[2].select = 1;
				g->xCMD = acpAUT_WRITE;
				sm->Step ++;
			}	

		break;


		case 23: sm_text("W: State 2" ,sm);

			if(g->Status.ACT_STATE_Indx == 2){

				g->xCMD = acpAUT_RESETSIGNAL1;
				sm->Step  = 20;
			}	

		break;


		case 30: sm_text("Jogging" ,sm);

			m->Status.Status = STAT_JOG;


			vaxis = g->msc.pVaxis;

			if(m->Cmd.JogFWD){

				if(g->Status.VaxMoveMode == ncOFF){

					if(vaxis){
						
						vaxis->move.basis.parameter.v_pos = (mach->pCurCfg->JogVel /mach->pCurCfg->BlankLen) * FULL_ROT;
						vaxis->move.basis.parameter.a1_pos = mach->pCurCfg->JogAcc;
						vaxis->move.basis.parameter.a2_pos = vaxis->move.basis.parameter.a1_pos;
						
						g->Command.VaxMoveVelocity = 1;

					}

				}
			}else {
			
				g->Command.VaxStop= 1;

				sm->Step  = 31;

			}	


			if(m->Cmd.Resynchro){

				m->Cmd.JogFWD = 0;	
				g->Command.VaxMoveVelocity = 0;
				g->Command.VaxStop = 1;
				
				sm->Step  = 32;

			}	
				

		break;


		case 31: sm_text("Stop Jog" ,sm);

			m->Status.Status = STAT_STOP_JOG;			

			if(m->Cmd.Resynchro){

				m->Cmd.JogFWD = 0;
				g->Command.VaxStop = 1;
				
				sm->Step  ++;

			}	
			else if(m->Cmd.JogFWD)
				sm->Step  = 30;	
				


		break;

		case 32: sm_text("W: Stop" ,sm);

			m->Status.Status = STAT_RESYN;

			
			if(g->Status.VaxMoveMode == ncOFF){
				
				vaxis = g->msc.pVaxis;

				if(vaxis){

					vaxis->move.basis.parameter.s = g->Status.VaxActPos -  (g->Status.VaxActPos%FULL_ROT);
					vaxis->move.basis.parameter.s += g->msc.pCurCfg->start_offset + m->msc.startOffset;		
					
					vaxis->move.basis.parameter.v_pos = (mach->pCurCfg->AlignVel /mach->pCurCfg->BlankLen) * FULL_ROT;
					vaxis->move.basis.parameter.a1_pos = mach->pCurCfg->AlignAcc;
					vaxis->move.basis.parameter.a2_pos = vaxis->move.basis.parameter.a1_pos;

					g->xCMD = acpVAX_ABS_MOVE;
				}
				sm->Step  ++;

			}	

		break;	

		case 33: sm_text("W: Resynch" ,sm);

			if(g->Status.VaxMoveMode == ncOFF)
				sm->Step = 20;


		break;

		

				

	}

	/////////////////////

	/********************************************************/		
		
	memcpy(&g->Param.PlnkDataTo[0], &g->Param.MPGEN_SET, 4); //MPGEN
	memcpy(&g->Param.PlnkDataTo[4],  &g->Param.COMP_SL_S, 4);
	memcpy(&g->Param.PlnkDataTo[8],  &g->Param.SL_FACTOR, 4);	

	/********************************************************/
	
	sm_serv(sm);
	TimerExecute(&m->msc.tmr1);
	TimerExecute(&m->msc.tmr2);
	TimerExecute(&m->msc.watchDog);



}

DINT AbsFromDINT(DINT x){

	if( x > 0 )
		return x;
	else
		return (  x* (-1));


}
