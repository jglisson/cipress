#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/*********************************************************
* FILE	 		:	Screen.c
*
* OBJECTS		:
*
* OVERVIEW		:
*
* HISTORY 		:
*
* DEPENDANCIES	:
*********************************************************/

#ifdef __cplusplus
	extern "C"
	{
#endif
#include "ACPx.h"
#ifdef __cplusplus
	};
#endif

#include <bur/plc.h>
#include <bur/plctypes.h>
#include <math.h>
#include "standard.h"
#include <..\Glob\globaldef.h>
#include <..\Glob\globaldecl.h>
#include <..\Libraries\StateLib\timer_lib.h>

/* LOCAL TYPES */

/* GLOBAL VARIABLES: */

/* MUST BE LOCAL */

_LOCAL	plcstring	SoftVersion[32];

/////////////////////////////

_LOCAL ACP_typ *Servo; // For servo Page 
_LOCAL SM_obj		 	*HMI_AXIS_sm;
_LOCAL AXIS_Color_typ  	*HMI_mAXIS;
_LOCAL ACP_CurCfg_typ   *HMI_ACP_CurCfg;
_LOCAL float HMI_StartOffset;

/* NON PV Space */
TON_typ *t1,*t2,*t3;
SM_obj *sm;
TON_typ		screen_t[3];
unsigned short _ReadPage; 

/* NON PV Space */

_LOCAL UINT	_ReadPage;
_LOCAL USINT	ready;
_LOCAL UDINT	VC_HANDLE;
_LOCAL UINT	Access_Status, GetActAlarmList_Status;
_LOCAL USINT	AlarmString[80];
_LOCAL UDINT	AlarmStringAdr;
_LOCAL UINT	Alarmlen, AlarmTruncate, AlrMsgLeftTrunc;
_LOCAL USINT	Seperator;
_LOCAL USINT	TimeDateForm;
_LOCAL USINT	Alarmtype;

/* For calibration */
_LOCAL DATE_AND_TIME dateTime;
_LOCAL DTStructure DateTimeActual;
_LOCAL BOOL changeDateTime;
_LOCAL DTSetTime_typ DTSetTime_1;
_LOCAL DTGetTime_typ DTGetTime_1;
_LOCAL	UINT 		AlrMsgTrunc;

/////////////////////////


/* PROTOTYPES: */

DINT Screen_serv( SCREEN_obj *s );
void Scr00_INIT();
void Scr10_MAIN();
void Scr11_MAIN_IND();
void Scr30_ALARM();
void Scr31_ALARM1();
void Scr40_IND_COLOR();
void Scr60_SETUP();

void Scr80_SERVO();
void	Scr81_SERVODIAGN();

void Scr100_CALIBRATION();

UINT CreateBtnColor( UINT TextColor, UINT ButtonColor );
void ResetButtons();
UINT GetBarVis( USINT vis, BOOL enable );
void MenuBar();
void VisBar( SCREEN_obj *s, UINT status, USINT active );
void InVisBar( SCREEN_obj *s, UINT status, USINT active );
void Vis( SCREEN_obj *s, UINT status, USINT active );
void InVis( SCREEN_obj *s, UINT status, USINT active );
void SR_BypassJam (INT offset);
void SR_BypassPhJam (INT offset);
void SR_BypassCover (INT offset);
void SR_AUTO(INT offset);
void SR_TOTALJAM();
void BtnScreenView();



/* DEFINITIONS: */


/*=== SCREEN NAMES ===*/

#define SCR00_INIT			00
#define SCR10_MAIN 			10
#define SCR11_MAIN_IND 		11
#define SCR30_ALARM			30
#define SCR31_ALARM1		31
#define SCR40_IND_COLOR	40
#define SCR60_SETUP			60


#define SCR80_SERVO		 	80
#define SCR81_SERVODIAGN	81

#define SCR100_CALIBRATION	100


/*===COLORS===*/

#define GREEN				10
#define DARK_GREEN			2
#define BLUE				1
#define BABY_BLUE			11
#define TEAL				168
#define RED					0x33
#define YELLOW				46
#define ORANGE				48
#define BLACK				0
#define WHITE				15
#define GRAY				7
#define DARK_NAVY			219
#define DARK_GRAY			8
#define LIGHT_GRAY			227
#define CYAN				196
#define MAGENTA				21
#define BROWN				86
#define LIGHT_BLUE			52
#define CHECK				1
#define UNCHECK				0
#define VISIBLE				0
#define INVISIBLE			1
#define YELLOW_ON_DBLUE		0x2EDB
#define DBLUE_ON_YELLOW		0xDB2E
#define WHITE_ON_RED		0x0F33
#define RED_ON_WHITE		0x330F
#define WHITE_ON_BLACK		0x0F00
#define BLACK_ON_WHITE		0x000F
#define BLACK_ON_LYELLOW	0x001C
#define BLACK_ON_MAGENTA	0x0015
#define YELLOW_ON_BLUE		0x2E01
#define BLUE_ON_YELLOW		0x012E
#define	ROLE_LEVEL_1		1

/* MACROS: */

#define STD_BUTTON		CreateBtnColor( BLACK, LIGHT_GRAY )
#define HL_BUTTON		CreateBtnColor( BLACK, YELLOW )
#define SELECT_TAB		CreateBtnColor( BLACK, DARK_GRAY)
#define DEFAULT_TAB		CreateBtnColor( BLACK, WHITE )
#define UNAVAIL_TAB		CreateBtnColor( GRAY, WHITE )

/* ***** */

_INIT void init(void){

	int i;

	/* Attach */

	sm 	= &Screen_sm;
	t1	= &screen_t[0];
	t2	= &screen_t[1];
	t3	= &screen_t[2];

	sm_init( &Screen_sm );

	strcpy(SoftVersion , SOFTWARE_VERSION);

	/* invisible all squares on layout screen */

	Screen.Units = 2;
}

/* CYCLIC ROUTINE: */

_CYCLIC void cyclic(void){

	int i;

	Mach.pCurCfg = &CurCfg;

	
	MenuBar();

	/* Screen state machine */

	switch(Screen.ReadPage){

		case SCR00_INIT:
			Scr00_INIT();
		break;

		case SCR10_MAIN:
			Scr10_MAIN();
		break;	

		case SCR11_MAIN_IND:
			Scr11_MAIN_IND();
		break;	

		case SCR30_ALARM:
			Scr30_ALARM();
		break;	

		case SCR31_ALARM1:
			Scr31_ALARM1();
		break;

		case SCR40_IND_COLOR:
			Scr40_IND_COLOR();
		break;	

		case SCR60_SETUP:
			Scr60_SETUP();
		break;	

		case SCR81_SERVODIAGN:
			Scr81_SERVODIAGN();
		break;	

		case SCR100_CALIBRATION:
			Scr100_CALIBRATION();
		break;
	}

	/* Screen Serv execution */

	Screen_serv(&Screen);

	ResetButtons();
}

/* ***** */

void Scr00_INIT(){

	/* State Machine */

	BEGIN_SM

		case 0:
			TB "Please Wait: " TE
			/* Hold */
			if ((Screen.ReadPage == SCR00_INIT) && (Screen.SetPage == 0xFFFF)){
				GO_BASE_SM2;
			}
		break;

		case BASE_SM2:
			TB "Ready" TE
		break;

	END_SM
}

/* ***** */


void	Scr10_MAIN(){
int i ;
	
	BEGIN_SM

		case BASE_SM:
			TB "Initializing Page" TE

			/* Hold */

			if ((Screen.ReadPage == SCR10_MAIN) && (Screen.SetPage == 0xFFFF)){

				
				InVis(&Screen, VISIBLE, 0); 
				INC_SM;
			}
		break;

		case 1:
			TB "Checking Security" TE

				
			GO_BASE_SM2;
		break;

		case BASE_SM2:
			TB "Ready" TE

			///////////////////////////////////////////////////
			/* Display Current Alarm on Banner */

			if (ready == 0){

			  VC_HANDLE = VA_Setup (1,"Visu");
			  if (VC_HANDLE != 0){
				ready = 1;
				AlrMsgTrunc = 5;
				AlrMsgLeftTrunc = 23;

				/*
				x1 = 475;
			    y1 = 65;
			    x2 = 25;
			    y2 = 200;
			    fill = 255; 
			    color = 0; 
			    */
			  }
			}
			else{
			  Access_Status = VA_Saccess (1,VC_HANDLE);
			  if (Access_Status == 0){
			  	Alarmlen = 80;
			  	Seperator = 45; /*Hyphen*/
			  	Alarmtype = 1; /*First alarm*/
			  	TimeDateForm = 1; /*DD.MM.YY HH.MM.SS*/
			  	GetActAlarmList_Status =  VA_GetActAlarmList (1, VC_HANDLE,AlarmString, &Alarmlen, Alarmtype ,Seperator,TimeDateForm);
				AlarmStringAdr =(UDINT) &AlarmString[0];
				AlarmStringAdr = AlarmStringAdr + (UDINT)AlrMsgLeftTrunc;  
				AlarmString[Alarmlen - AlrMsgTrunc] = 0;
			  	//AlarmStringAdr = RIGHT (AlarmString , (Alarmlen - AlrMsgTrunc));
			  	//AlarmStringAdr = LEFT (AlarmStringAdr , Alarmlen - AlrMsgTrunc - AlrMsgLeftTrunc);
			  	strcpy(Screen.AlarmMsg ,AlarmStringAdr);
			  	VA_Srelease (1,VC_HANDLE);
			  }
			}

			/////////////////////////////////////////////////////


				switch(Screen.GlbBtn){

					//  FWD & REV Command
					case CMD_FWD_FINE:

						for(i= 0 ; i < COLOR_NUM ; i++)
							Mach.Cmd.PrintCylind[i] = CMD_FWD_FINE;// 

					break;	

					case CMD_REV_FINE:

						for(i= 0 ; i < COLOR_NUM ; i++)
							Mach.Cmd.PrintCylind[i] = CMD_REV_FINE;// 

					break;

					case CMD_FWD_COARSE:

						for(i= 0 ; i < COLOR_NUM ; i++)
							Mach.Cmd.PrintCylind[i] = CMD_FWD_COARSE;// 

					break;

					case CMD_REV_COARSE:

						for(i= 0 ; i < COLOR_NUM ; i++)
							Mach.Cmd.PrintCylind[i] = CMD_REV_COARSE;// 

					break;

				}
				Screen.GlbBtn = 0;
			
				/************************/
				for( i = 0 ; i < COLOR_NUM ; i++){
					
					if(Screen.btn[i]){
						
						Screen.Index = i;
						Screen.GotoScreen = SCR11_MAIN_IND;
					}	

				}
				if(Screen.Index >= COLOR_NUM)
					Screen.Index = 1;

				/**********************/

				for( i = 0 ; i < COLOR_NUM ; i++){

					if(Mach.pCurCfg->Color_Active[i])
						Screen.vis[i] = VISIBLE;
					else
						Screen.vis[i] = INVISIBLE;			

				}				
				
			
		break;

		case 30:
		break;

		/* ***** */

		case ERROR_SM:
			TB "Error" TE
		break;

	END_SM
}

/* ***** */
void Scr11_MAIN_IND(){
int i ;
	
	BEGIN_SM

		case BASE_SM:
			TB "Initializing Page" TE

			/* Hold */

			if ((Screen.ReadPage == SCR11_MAIN_IND) && (Screen.SetPage == 0xFFFF)){
			
				InVis(&Screen, VISIBLE, 0); 
				INC_SM;
			}
		break;

		case 1:
			TB "Checking Security" TE

				
			GO_BASE_SM2;
		break;

		case BASE_SM2:
			TB "Ready" TE

			/////////////////////////////////////////////////////

				Mach.Cmd.PrintCylind[Screen.Index] = 0;
						
				switch(Screen.GlbBtn){

					//  FWD & REV Command
					case CMD_FWD_FINE:

						Mach.Cmd.PrintCylind[Screen.Index] = CMD_FWD_FINE;// 
						Screen.GlbBtn = 0;

					break;	

					case CMD_REV_FINE:

						Mach.Cmd.PrintCylind[Screen.Index] = CMD_REV_FINE;// 
						Screen.GlbBtn = 0;

					break;

					case CMD_FWD_COARSE:

						Mach.Cmd.PrintCylind[Screen.Index] = CMD_FWD_COARSE;// 
						Screen.GlbBtn = 0;

					break;

					case CMD_REV_COARSE:

						Mach.Cmd.PrintCylind[Screen.Index] = CMD_REV_COARSE;// 
						Screen.GlbBtn = 0;

					break;

					//////// ANILOX ROLLER  ON OFF AUTO  //////////

					case CMD_OFF:

						Mach.pCurCfg->AniloxRoller_Mode[Screen.Index] = OFF;
						Screen.GlbBtn = 0;

					break;	

					case CMD_ON:

						Mach.pCurCfg->AniloxRoller_Mode[Screen.Index] = ON;
						Screen.GlbBtn = 0;

					break;	

					case CMD_AUTO:

						Mach.pCurCfg->AniloxRoller_Mode[Screen.Index] = AUTO;
						Screen.GlbBtn = 0;

					break;

					//////// DR BLADE  ON OFF AUTO  //////////

					case CMD_OFF + 3:

						Mach.pCurCfg->DrBlade_Mode[Screen.Index] = OFF;
						Screen.GlbBtn = 0;

					break;	

					case CMD_ON + 3:

						Mach.pCurCfg->DrBlade_Mode[Screen.Index] = ON;
						Screen.GlbBtn = 0;

					break;	

					case CMD_AUTO + 3:

						Mach.pCurCfg->DrBlade_Mode[Screen.Index] = AUTO;
						Screen.GlbBtn = 0;

					break;

					//////// PRINT CYLINDER  ON OFF AUTO  //////////

					case CMD_OFF + 6:

						Mach.pCurCfg->PrintCylind_Mode[Screen.Index] = OFF;
						Screen.GlbBtn = 0;

					break;	

					case CMD_ON + 6:

						Mach.pCurCfg->PrintCylind_Mode[Screen.Index] = ON;
						Screen.GlbBtn = 0;

					break;	

					case CMD_AUTO + 6:

						Mach.pCurCfg->PrintCylind_Mode[Screen.Index] = AUTO;
						Screen.GlbBtn = 0;

					break;

					
					//////////  JOG PRINT CYLINDER  //////////////////////

					case CMD_JOG_POSITIVE+6:

						Mach.Cmd.PrintCylind[Screen.Index] = CMD_JOG_POSITIVE;
						// Should be connected to phisical button on machine

					break;
					

					//////////  Accept Position   //////////////////////

					case CMD_ACCEPT_POS+6:

						Mach.Cmd.PrintCylind[Screen.Index] = CMD_ACCEPT_POS;
						Screen.GlbBtn = 0;

					break;


					//////// INP PUMP  ON OFF  //////////

					case CMD_OFF + 11:

						Mach.pCurCfg->InkPump_Mode[Screen.Index] = OFF;
						Screen.GlbBtn = 0;

					break;	

					case CMD_ON + 11:

						Mach.pCurCfg->InkPump_Mode[Screen.Index] = ON;
						Screen.GlbBtn = 0;

					break;	

					///////////////////////////////////////////////////

					default:

						Screen.GlbBtn = 0;	


					break;	


				}

				

				////////////  FEEDBACK  /////////////////////

				switch(Mach.pCurCfg->AniloxRoller_Mode[Screen.Index]){

					case OFF:

						Screen.color[CMD_OFF] 	= 1;
						Screen.color[CMD_ON] 		= 20;
						Screen.color[CMD_AUTO] 	= 30;

					break;	

					case ON:

						Screen.color[CMD_OFF] 	= 10;
						Screen.color[CMD_ON] 		= 2;
						Screen.color[CMD_AUTO] 	= 30;

					break;	

					case AUTO:

						Screen.color[CMD_OFF] 	= 10;
						Screen.color[CMD_ON] 		= 20;
						Screen.color[CMD_AUTO] 	= 3;

					break;	


				}	

				switch(Mach.pCurCfg->DrBlade_Mode[Screen.Index]){

					case OFF:

						Screen.color[CMD_OFF+3] 		= 1;
						Screen.color[CMD_ON+3] 		= 20;
						Screen.color[CMD_AUTO+3] 	= 30;

					break;	

					case ON:

						Screen.color[CMD_OFF+3] 		= 10;
						Screen.color[CMD_ON+3] 		= 2;
						Screen.color[CMD_AUTO+3] 	= 30;

					break;	

					case AUTO:

						Screen.color[CMD_OFF+3] 		= 10;
						Screen.color[CMD_ON+3] 		= 20;
						Screen.color[CMD_AUTO+3] 	= 3;

					break;	


				}	

				switch(Mach.pCurCfg->PrintCylind_Mode[Screen.Index]){

					case OFF:

						Screen.color[CMD_OFF+6] 		= 1;
						Screen.color[CMD_ON+6] 		= 20;
						Screen.color[CMD_AUTO+6] 	= 30;

					break;	

					case ON:

						Screen.color[CMD_OFF+6] 		= 10;
						Screen.color[CMD_ON+6] 		= 2;
						Screen.color[CMD_AUTO+6] 	= 30;

					break;	

					case AUTO:

						Screen.color[CMD_OFF+6] 		= 10;
						Screen.color[CMD_ON+6] 		= 20;
						Screen.color[CMD_AUTO+6] 	= 3;

					break;	


				}	


				switch(Mach.pCurCfg->InkPump_Mode[Screen.Index]){

					case OFF:

						Screen.color[CMD_OFF+9] 		= 1;
						Screen.color[CMD_ON+9] 		= 20;
						//Screen.color[CMD_AUTO+9] 	= 30;

					break;	

					case ON:

						Screen.color[CMD_OFF+9] 		= 10;
						Screen.color[CMD_ON+9] 		= 2;
						//Screen.color[CMD_AUTO+9] 	= 30;

					break;						


				}	
				
				
				Screen.color[0] = Screen.Index;  //  Color Name
				Screen.color[1] = Mach.Status.PrintCylind[Screen.Index]; // Color status
				Screen.color[2] = Mach.Status.AniloxRoller[Screen.Index]; // Color status
				Screen.color[3] = Mach.Status.InkPump[Screen.Index]; // Color status
			
			/****************************/

			HMI_AXIS_sm = &AXIS_sm[Screen.Index + COLOR_1];
			HMI_mAXIS = &mAXIS[Screen.Index + COLOR_1];

			

			/************************/			
			
			
		break;

		case 30:
		break;

		/* ***** */

		case ERROR_SM:
			TB "Error" TE
		break;

	END_SM
}
/*************/

void Scr30_ALARM(){

	
	BEGIN_SM

		case BASE_SM:
			TB "Initializing Page" TE

			/* Hold */

			if ((Screen.ReadPage == SCR30_ALARM) && (Screen.SetPage == 0xFFFF)){
				InVis(&Screen, VISIBLE, 0); 
				INC_SM;
			}
		break;

		case 1:
			TB "Checking Security" TE
			GO_BASE_SM2;
		break;

		case BASE_SM2:
			TB "Ready" TE
			
		break;

		case 30:
		break;

		/* ***** */

		case ERROR_SM:
			TB "Error" TE
		break;

	END_SM
	
}


/* ***** */
void Scr31_ALARM1(){

	
	BEGIN_SM

		case BASE_SM:
			TB "Initializing Page" TE

			/* Hold */

			if ((Screen.ReadPage == SCR31_ALARM1) && (Screen.SetPage == 0xFFFF)){
				InVis(&Screen, VISIBLE, 0); 
				INC_SM;
			}
		break;

		case 1:
			TB "Checking Security" TE
			GO_BASE_SM2;
		break;

		case BASE_SM2:
			TB "Ready" TE
			
		break;

		case 30:
		break;

		/* ***** */

		case ERROR_SM:
			TB "Error" TE
		break;

	END_SM
}


/* ***** */
void Scr40_IND_COLOR(){

	
	BEGIN_SM

		case BASE_SM:
			TB "Initializing Page" TE

			/* Hold */

			if ((Screen.ReadPage == SCR40_IND_COLOR) && (Screen.SetPage == 0xFFFF)){
				InVis(&Screen, VISIBLE, 0); 
				INC_SM;
			}
		break;

		case 1:
			TB "Checking Security" TE
			GO_BASE_SM2;
		break;

		case BASE_SM2:
			TB "Ready" TE
			
		break;

		case 30:
		break;

		/* ***** */

		case ERROR_SM:
			TB "Error" TE
		break;

	END_SM
}


/* ***** */
void Scr60_SETUP(){

	
	BEGIN_SM

		case BASE_SM:
			TB "Initializing Page" TE

			/* Hold */

			if ((Screen.ReadPage == SCR60_SETUP) && (Screen.SetPage == 0xFFFF)){
				InVis(&Screen, VISIBLE, 0); 
				INC_SM;
			}
		break;

		case 1:
			TB "Checking Security" TE

			DTGetTime_1.enable = 1;
			DTGetTime(&DTGetTime_1);
			DT_TO_DTStructure(DTGetTime_1.DT1, (UDINT) &DateTimeActual);

			GO_BASE_SM2;
		break;

		case BASE_SM2:
			TB "Ready" TE

			 if(changeDateTime){
			 	changeDateTime = 0;

				
				dateTime =DTStructure_TO_DT((UDINT) &DateTimeActual);

				DTSetTime_1.enable = 1;
				DTSetTime_1.DT1 = dateTime;
				DTSetTime(&DTSetTime_1);
				
			 }
		 			
		break;

		case 30:
		break;

		/* ***** */

		case ERROR_SM:
			TB "Error" TE
		break;

	END_SM
}


/****** */
void	Scr80_SERVO(){

float incr;


		
	/////////////////////////////////////////////////////////////
	
	/* State Machine */

	BEGIN_SM

		case 0:
			TB "Please Wait: " TE

			/* Hold */

			if ((Screen.ReadPage == SCR80_SERVO) && (Screen.SetPage == 0xFFFF)){
				GOTO_SM 1;				
			}
		break;

		case 1: 
			TB "Calculate FBK " TE

			
			GO_BASE_SM2;

		break;	

		case BASE_SM2:
			TB "Ready" TE

			
		break;

		

	END_SM
}

/* ***** */
void	Scr81_SERVODIAGN(){
USINT i;
	/* State Machine */

	BEGIN_SM

		case 0:
			TB "Please Wait: " TE

			/* Hold */

			if ((Screen.ReadPage == SCR81_SERVODIAGN) && (Screen.SetPage == 0xFFFF)){
				Screen.Index = 0;
				GO_BASE_SM2;
			} 
		break;

		case BASE_SM2:
			TB "Ready" TE

			if(Screen.Index < SERVO_NUM){
				
				Servo = &gAxis[Screen.Index];

				HMI_ACP_CurCfg = &ServoCurCfg[Screen.Index];
				HMI_AXIS_sm = &AXIS_sm[Screen.Index];
				HMI_mAXIS = &mAXIS[Screen.Index];

		
			}
			else
				Screen.Index = 0;

			/** Selected Servo button colo */
			for(i= 0 ; i < SERVO_NUM;i++)
				Screen.color[i] = DARK_GREEN;
			
			Screen.color[Screen.Index] = GREEN;

			/***************************/

			if(Screen.btn[0])
				Servo->Command.ErrorAck = 1;			
			
		break;

	END_SM
}

/*****************************/

/* ***** */
 void Scr100_CALIBRATION(){


	 if(changeDateTime){
	 	changeDateTime = 0;

		
		dateTime =DTStructure_TO_DT((UDINT) &DateTimeActual);

		DTSetTime_1.enable = 1;
		DTSetTime_1.DT1 = dateTime;
		DTSetTime(&DTSetTime_1);
		
	 }
	 else{

		DTGetTime_1.enable = 1;
		DTGetTime(&DTGetTime_1);
		DT_TO_DTStructure(DTGetTime_1.DT1, (UDINT) &DateTimeActual);

	 }	


}




void  MenuBar()
{
	if ( Screen.GotoScreen < 0xFFFF ) {
		if( Screen.GotoScreen != Screen.ReadPage ) {

			/* Reset Vis */

			Screen.SetInVisible	= ON;
			Screen.ReturnScreen	= Screen.ReadPage;
			RESET_SM;
			Screen.SetPage		= Screen.GotoScreen;
			//Screen.Index		= 0;

		}
		Screen.GotoScreen 	= 0xFFFF;
	}

	/* Reset Screen Sm every new screen is set from Function key */

	if(Screen.ReadPage != _ReadPage){
		Screen.GotoScreen 	= 0xFFFF;
		Screen_sm.Step 		= 0;
	}
	_ReadPage = Screen.ReadPage;
return;
}

/* ***** */

DINT Screen_serv( SCREEN_obj *s ){


	
	
	

	return 0;
}

/* ***** */

UINT CreateBtnColor( UINT TextColor, UINT ButtonColor ){
	return ((TextColor << 8) | ButtonColor);
}

/* ***** */

void ResetButtons()
{
USINT i;

	/* Buttons 40+ are momentary */

	for (i=0;i<40;i++)
		Screen.btn[i]	= OFF;

	/* Master AUTO Buttons 5 */
	
	/* ***** */

	//btnLubTest=0;

	/* Bar Buttons 30 */

	for (i=0;i<30;i++)
		Screen.btnBar[i]	= OFF;

	/* Bar Buttons 10 */

	for (i=0;i<10;i++)
		Screen.btnMenu[i] = OFF;
return;
}

/* ***** */

UINT GetBarVis( USINT vis, BOOL enable ){

	if ( (vis > 0) && (enable))
		return VISIBLE;

	return INVISIBLE;
}

/* ***** */

void VisBar( SCREEN_obj *s, UINT status, USINT active ){
USINT i;

	for (i=0;i<7;i++)	s->visBar[i]	= status;
	s->visBar[active]	= VISIBLE;
}

/* ***** */

void InVisBar( SCREEN_obj *s, UINT status, USINT active ){
USINT i;

	for (i=0;i<10;i++)	s->visBar[i]	= status;
	s->visBar[active]	= INVISIBLE;
}

/* ***** */

void Vis( SCREEN_obj *s, UINT status, USINT active ){
USINT i;

	for (i=0;i<30;i++)	s->vis[i]	= status;
	s->vis[active]	= VISIBLE;
}

/* ***** */

void InVis( SCREEN_obj *s, UINT status, USINT active ){
USINT i;

	for (i=0;i<30;i++)	s->vis[i]	= status;
	s->vis[active]	= INVISIBLE;
}

/* ***** */




